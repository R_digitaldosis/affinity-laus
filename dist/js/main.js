'use strict';
jQuery(document).ready(function($) {
  var breakpoint = window.getCurrentBreakpoint();
  var header = $('.js-header').height();
  var sliders = {};

  var detectScroll = function() {
    var lastScrollTop = 0;

    $(window).scroll(function() {
      var st = $(this).scrollTop();

      if (st > lastScrollTop && st >= header) {
        $('.js-header').addClass('is-up');
        setTimeout(function() {
          $('.js-header').addClass('is-fixed');
        }, 200);
      } else if (st <= 2) {
        $('.js-header').removeClass('is-up is-fixed');
        setTimeout(function() {
          $('.js-header').removeClass('is-fixed');
        }, 200);
      } else {
        $('.js-header').removeClass('is-up');
      }
      lastScrollTop = st;
    });
  };

  // TODO: trigger on content load
  var showLoader = function() {
    setTimeout(function() {
      $('.js-loader').addClass('loading');
    }, 500);
    setTimeout(function() {
      $('.js-loader')
        .removeClass('loading')
        .addClass('loaded loading--exit');
    }, 1500);
  };
  var exitLoader = function() {
    $('.js-loader').removeClass('loaded loading--exit');
  };

  var disableArrows = function(slider, arrow) {
    var $prev = $(arrow + '-prev');
    var $next = $(arrow + '-next');
    $prev.removeClass('is-disabled');
    $next.removeClass('is-disabled');
    if (slider.isBeginning) {
      $prev.addClass('is-disabled');
    }
    if (slider.isEnd) {
      $next.addClass('is-disabled');
    }
  };

  var historySwiper = function() {
    if ($('.js-history-slider').length) {
      if (sliders.history) {
        sliders.history.destroy(true, true);
      }
      sliders.history = new window.Swiper('.js-history-slider', {
        slidesPerView: 'auto',
      });
      disableArrows(sliders.history, '.js-history-slider');
    }
  };
  var testimonialSwiper = function() {
    if ($('.js-testimonials-slider').length) {
      if (sliders.testimonial) {
        sliders.testimonial.destroy(true, true);
      }
      sliders.testimonial = new window.Swiper('.js-testimonials-slider', {
        slidesPerView: 'auto',
      });
      disableArrows(sliders.testimonial, '.js-testimonials-slider');
    }
  };

  var hiringSwiper = function() {
    if ($('.js-hiring-slider').length) {
      if (sliders.hiring) {
        sliders.hiring.destroy(true, true);
      }
      sliders.hiring = new window.Swiper('.js-hiring-slider', {
        slidesPerView: 'auto',
      });
      disableArrows(sliders.hiring, '.js-hiring-slider');
    }
  };

  var resize = function() {
    if (breakpoint !== window.getCurrentBreakpoint()) {
      historySwiper();
    }
  };
  var filters = {
    location: [],
    category: []
  };

  var applyFilters = function() {
    $('.js-opening').addClass('is-hidden');
    $('.js-no-results').removeClass('is-visible');

    filters.location.forEach(function(location) {
      if (filters.category.length) {
        filters.category.forEach(function(category) {
          $('.js-opening[data-location="' + location + '"][data-category="' + category + '"]').removeClass('is-hidden');
        });
      } else {
        $('.js-opening[data-location="' + location + '"]').removeClass('is-hidden');
      }
    });

    filters.category.forEach(function(category) {
      if (filters.location.length) {
        filters.location.forEach(function(location) {
          $('.js-opening[data-location="' + location + '"][data-category="' + category + '"]').removeClass('is-hidden');
        });
      } else {
        $('.js-opening[data-category="' + category + '"]').removeClass('is-hidden');
      }
    });

    if (!filters.location.length && !filters.category.length) {
      $('.js-opening').removeClass('is-hidden');
    }
    if (!$('.js-opening:visible').length) {
      $('.js-no-results').addClass('is-visible');
    }
  };
  var showCookies = function() {
    if (document.cookie.indexOf('accept=true') === -1) {
      $('.js-cookies').addClass('is-visible');
    }
  }


  var handleEvents = function() {
    // Open menu
    $('.js-open-menu').on('click', function(e) {
      e.preventDefault();
      $(this).addClass('is-active');
      $('.js-close-menu').addClass('is-active');
      $('.js-menu').addClass('is-visible');
    });

    // Close menu
    $('.js-close-menu').on('click', function(e) {
      e.preventDefault();
      $(this).removeClass('is-active');
      $('.js-open-menu').removeClass('is-active');
      $('.js-menu').removeClass('is-visible');
    });

    // Language selector
    $('.js-language-selector').on('click', function(e) {
      e.preventDefault();
      $(this).toggleClass('is-selected');
    });

    // Window resize
    $(window).on('resize', function() {
      header = $('.js-header').height();
      resize();
      breakpoint = window.getCurrentBreakpoint();
    });

    // History slider prev
    $('.js-history-slider-prev').on('click', function() {
      if (sliders.history) {
        sliders.history.slidePrev();
        disableArrows(sliders.history, '.js-history-slider');
      }
    });

    // History slider next
    $('.js-history-slider-next').on('click', function() {
      if (sliders.history) {
        sliders.history.slideNext();
        disableArrows(sliders.history, '.js-history-slider');
      }
    });

    // Testimonials slider prev
    $('.js-testimonials-slider-prev').on('click', function() {
      if (sliders.testimonial) {
        sliders.testimonial.slidePrev();
        disableArrows(sliders.testimonial, '.js-testimonials-slider');
      }
    });

    // Testimonials slider next
    $('.js-testimonials-slider-next').on('click', function() {
      if (sliders.testimonial) {
        sliders.testimonial.slideNext();
        disableArrows(sliders.testimonial, '.js-testimonials-slider');
      }
    });

    // Hiring slider prev
    $('.js-hiring-slider-prev').on('click', function() {
      if (sliders.hiring) {
        sliders.hiring.slidePrev();
        disableArrows(sliders.hiring, '.js-hiring-slider');
      }
    });

    // Hiring slider next
    $('.js-hiring-slider-next').on('click', function() {
      if (sliders.hiring) {
        sliders.hiring.slideNext();
        disableArrows(sliders.hiring, '.js-hiring-slider');
      }
    });

    //internships-cases show text
    $('.grid-opportunities-cases__arrow-down').on('click', function() {
      $(this)
        .closest('.grid-opportunities-cases')
        .toggleClass('grid-opportunities-cases--extended');
    });

    // Open lightbox
    $('.js-open-lightbox').on('click', function(e) {
      e.preventDefault();
      $('.js-lightbox[data-lightbox="' + $(this).data('lightbox') + '"]').addClass('is-visible');
    });

    // Close lightbox
    $('.js-close-lightbox').on('click', function() {
      $('.js-lightbox').removeClass('is-visible');
    });

    // Offices hover
    $('.js-location-hover').on('mouseenter', function() {
      $('.js-location-hover').addClass('is-gray');
      $('.js-location-hover[data-location="' + $(this).data('location') + '"]').removeClass('is-gray');
    });
    $('.js-location-hover').on('mouseleave', function() {
      $('.js-location-hover').removeClass('is-gray');
    });

    // Scroll to
    $('.js-scroll-to').on('click', function(e) {
      var selector = $(this).data('scroll');
      e.preventDefault();
      $('html,body').animate(
        {
          scrollTop: $(selector).offset().top,
        },
        500
      );
    });

    // Loader exit
    $('.js-exit-loader').on('click', function(e) {
      var destination = $(this).attr('href');
      e.preventDefault();
      exitLoader();
      setTimeout(function() {
        window.location.href = destination;
      }, 1500);
    });

    // Career filters
    $('.js-filter').change(function() {
      var filter = Object.keys(this.dataset)[0];
      var value = Object.values(this.dataset)[0];

      if ($(this).prop('checked')) {
        if ($.inArray(value, filters[filter]) === -1) {
          filters[filter].push(value);
        }
      } else {
        var index = filters[filter].indexOf(value);
        filters[filter].splice(index, 1);
      }

      if (breakpoint === 'desktop') {
        applyFilters();
      }
    });

    // Mobile career filters
    $('.js-apply-filters').click(function(e) {
      e.preventDefault();
      applyFilters();
    });
    $('.js-open-filters').click(function(e) {
      $('.js-filters-popup').addClass('is-visible');
    });
    $('.js-close-filters').click(function(e) {
      $('.js-filters-popup').removeClass('is-visible');
    });

    // Accept cookies
    $('.js-accept-cookies').on('click', function() {
      document.cookie = 'accept=true';
      $('.js-cookies').removeClass('is-visible');
    });

  };


  handleEvents();
  detectScroll();
  historySwiper();
  testimonialSwiper();
  hiringSwiper();
  showLoader();
  showCookies();
  console.log('ready');

});
