'use strict';
// window.controller = new ScrollMagic.Controller();
var wow = CustomEase.create('custom', 'M0,0 C0.126,0.382 0.139,0.691 0.314,0.82 0.412,0.892 0.818,1 1,1');
var height = window.innerHeight;
var speed1 = [
  // '.grid-cover .grey-1',
  // '.grid-cover .grey-4',
  // Home
  /*'.featured-grid .grey-1',
  '.featured-grid .grey-4',
  '.featured-grid .grey-7',
  '.featured-grid .grey-8',
  '.featured-grid .grey-10',
  '.grid-find-your-job .grid__item--grey',*/
  // About
  //'.our-mission .grey-2',
  '.our-history .grey-1',
  '.our-values .grey-3',
  '.our-location .grey-1',
  // Worklife
  // '.benefits .grey-2',
  '.benefits .grey-6',
  '.benefits .grey-8',
  '.testimonials .grey-2',
  '.testimonials .grey-3',
  '.testimonials .grey-6',
  // Internships
  '.grid-opportunities .grey-3',
];
var speed2 = [
  // '.grid-cover .grey-2',
  // Home
  /*'.featured-grid .grey-2',
  '.featured-grid .grey-5',
  '.featured-grid .grey-9',
  '.featured-grid .grey-12',*/
  // About
  '.our-mission .grey-1',
  '.our-mission .grey-3',
  '.our-mission .grey-5',
  '.our-history .grey-2',
  '.our-history .grey-3',
  '.our-values .grey-2',
  '.our-location .grey-3',
  // Worklife
  '.why-affinity .grey-1',
  '.benefits .grey-3',
  '.benefits .grey-5',
  '.benefits .grey-7',
  '.testimonials .grey-5',
  // Internships
  '.grid-opportunities .grey-2',
];
var speed3 = [
  // '.grid-cover .grey-3',
  // Home
  // '.featured-grid .grey-3',
  // '.featured-grid .grey-6',
  // '.featured-grid .grey-11',
  // About
  '.our-mission .grey-4',
  '.our-values .grey-1',
  '.our-values .grey-4',
  '.our-location .grey-2',
  // Worklife
  '.why-affinity .grey-2',
  // '.benefits .grey-1',
  '.benefits .grey-4',
  '.testimonials .grey-1',
  '.testimonials .grey-4',
  // Internships
  '.grid-opportunities .grey-1',
  '.grid-opportunities .grey-4',
];

var animateSquares = function() {
  speed1.forEach(function(selector) {

    var squareTween = TweenMax.to(selector, 1, {y: '-50%', ease: Linear.easeNone}, 0.8);
    var scene = new ScrollMagic.Scene({
      triggerElement: selector,
      triggerHook: 1,
      duration: '100%',
    })
    .setTween(squareTween)
    // .addIndicators({name: 'square ' + selector, colorEnd: '#f0f'});

    window.controller.addScene(scene);
  });
  speed2.forEach(function(selector) {

    var squareTween = TweenMax.to(selector, 2, {y: '-100%', ease: Linear.easeNone}, 0.4);
    var scene = new ScrollMagic.Scene({
      triggerElement: selector,
      triggerHook: 1,
      duration: '100%',
    })
    .setTween(squareTween)
    // .addIndicators({name: 'square ' + selector, colorEnd: '#0ff'});

    window.controller.addScene(scene);
  });
  speed3.forEach(function(selector) {

    var squareTween = TweenMax.to(selector, 3, {y: '-120%', ease: Linear.easeNone}, 1.2);
    var scene = new ScrollMagic.Scene({
      triggerElement: selector,
      triggerHook: 1,
      duration: '100%',
    })
    .setTween(squareTween)
    // .addIndicators({name: 'square ' + selector, colorEnd: '#f00'});

    window.controller.addScene(scene);
  });
};

var aboutAnimations = function() {
  var titles = [
    // '.our-mission__title',
    // '.our-history__title',
    // '.our-values__title',
    '.our-location__title',
  ];

  titles.forEach(function(selector) {
    var titleTween = TweenMax.fromTo(selector, 1, {y: '200%', ease: Linear.easeNone}, {y: '-100%', ease: Linear.easeNone}, 1);
    var scene = new ScrollMagic.Scene({
      triggerElement: selector,
      triggerHook: 1,
      duration: '100%',
    })
    .setTween(titleTween);
    window.controller.addScene(scene);
  });

  //Our mission
  //var cubeGreyGap = TweenMax.to('.our-mission .grey-2', 1, {y: '-200%', ease: Linear.easeNone});
  var missionTweenIn = TweenMax.fromTo('.our-mission__text.text-1', 1, {y: '100%', ease: Linear.easeNone}, {y: '50%', ease: Linear.easeNone});
  var missionTweenIn2 = TweenMax.fromTo('.our-mission__text.text-2', 1, {y: '100%', ease: Linear.easeNone}, {y: '40%', ease: Linear.easeNone});
  var missionTweenIn3 = TweenMax.fromTo('.our-mission__title', 1, {y: '700%', ease: Linear.easeNone}, {y: '400%', ease: Linear.easeNone});

  var missionTweenOut = TweenMax.fromTo('.our-mission__text.text-1', 1, {y: '50%', ease: Linear.easeNone}, {y: '0%', ease: Linear.easeNone})
  var missionTweenOut2 = TweenMax.fromTo('.our-mission__text.text-2', 1, {y: '40%', ease: Linear.easeNone}, {y: '-20%', ease: Linear.easeNone});
  var missionTweenOut3 = TweenMax.fromTo('.our-mission__title', 1, {y: '400%', ease: Linear.easeNone}, {y: '100%', ease: Linear.easeNone})

  /*var headerGap = new ScrollMagic.Scene({
    triggerElement: '.grid-cover--about',
    triggerHook: 0,
    duration: "25%",
  })
  .setTween(cubeGreyGap)

  var headerGap2 = new ScrollMagic.Scene({
    triggerElement: '.our-mission',
    triggerHook: 0.7,
    duration: "25%",
  })
  .setPin('.our-mission .grey-2')
  .addIndicators({name:'in'})
  .addTo(controller)*/

  var missionInTL = new TimelineMax();
  missionInTL.add([
    missionTweenIn,
    missionTweenIn2,
    missionTweenIn3
  ]);

  var missionOutTL = new TimelineMax();
  missionOutTL.add([
    missionTweenOut,
    missionTweenOut2,
    missionTweenOut3
  ]);

  var missionIn = new ScrollMagic.Scene({
    triggerElement: '.our-mission',
    triggerHook: 1,
    duration: height,
  })
  .setTween(missionInTL)
  //.addIndicators({name:'in'})

  var missionOut = new ScrollMagic.Scene({
    triggerElement: '.our-mission',
    triggerHook: 0,
    duration: height,
  })
  .setTween(missionOutTL)
  //.addIndicators({name:'out'})

  //window.controller.addScene(headerGap);
  window.controller.addScene(missionIn);
  window.controller.addScene(missionOut);


  //Our history
  var historyTweenIn = TweenMax.fromTo('.our-history__slider', 1, {y:'50%', ease: Linear.easeNone}, {y:'20%', ease: Linear.easeNone});
  var historyTweenIn2 = TweenMax.fromTo('.our-history__title', 1, {y:'600%', ease: Linear.easeNone}, {y:'300%', ease: Linear.easeNone});

  var historyTweenOut = TweenMax.fromTo('.our-history__slider', 1, {y:'20%', ease: Linear.easeNone}, {y:'-10%', ease: Linear.easeNone});
  var historyTweenOut2 = TweenMax.fromTo('.our-history__title', 1, {y:'300%', ease: Linear.easeNone}, {y:'0%', ease: Linear.easeNone});

  var historyInTL = new TimelineMax();
  historyInTL.add([
    historyTweenIn,
    historyTweenIn2
  ]);

  var historyOutTL = new TimelineMax();
  historyOutTL.add([
    historyTweenOut,
    historyTweenOut2
  ]);

  var historyIn = new ScrollMagic.Scene({
    triggerElement: '.our-history',
    triggerHook: 1,
    duration: height,
  })
  .setTween(historyInTL)
  // .addIndicators({name:'in'})

  var historyOut = new ScrollMagic.Scene({
    triggerElement: '.our-history',
    triggerHook: 0,
    duration: height,
  })
  .setTween(historyOutTL)
  // .addIndicators({name:'out'})

  window.controller.addScene(historyIn);
  window.controller.addScene(historyOut);

  // Timeline images
  var timeline = new ScrollMagic.Scene({
    triggerElement: '.our-history',
    triggerHook: 1,
    duration: height,
  })
  .setTween(TweenMax.from('.timeline__title, .timeline__text', 1, {y: height * 0.333, ease: Linear.easeNone}));

  window.controller.addScene(timeline);

  //Our values
  var valuesTweenIn = TweenMax.staggerFrom('.value', 1, {y:'100%', ease: Linear.easeNone}, 0.2);
  var valuesTweenIn2 = TweenMax.fromTo('.our-values__title', 1, {y:'300%', ease: Linear.easeNone}, {y:'0%', ease: Linear.easeNone});

  var valuesTweenOut = TweenMax.staggerTo('.value', 1, {y:'-100%', ease: Linear.easeNone}, 0.2);
  var valuesTweenOut2 = TweenMax.fromTo('.our-values__title', 1, {y:'0%', ease: Linear.easeNone}, {y:'-100%', ease: Linear.easeNone});


  var valuesInTL = new TimelineMax();
  valuesInTL.add([
    valuesTweenIn,
    valuesTweenIn2
  ]);

  var valuesOutTL = new TimelineMax();
  valuesOutTL.add([
    valuesTweenOut,
    valuesTweenOut2
  ]);

  var valuesIn = new ScrollMagic.Scene({
    triggerElement: '.our-values',
    triggerHook: 1,
    duration: height * 0.75,
  })
  .setTween(valuesInTL)
  // .addIndicators({name:'in'})

  var valuesOut = new ScrollMagic.Scene({
    triggerElement: '.our-values',
    triggerHook: 0,
    duration: height,
  })
  .setTween(valuesOutTL)
  // .addIndicators({name:'out'})

  window.controller.addScene(valuesIn);
  window.controller.addScene(valuesOut);

  // var values = new ScrollMagic.Scene({
  //   triggerElement: '.our-values',
  //   triggerHook: 1,
  //   duration: '100%',
  // })
  // .setTween(valuesTween)
  // window.controller.addScene(values);



  //Our location
  var locationsTween = TweenMax.staggerFrom('.our-location__image,.our-location__box', 2, {y:'50%', ease: Linear.easeNone}, 0.2);
  var locations = new ScrollMagic.Scene({
    triggerElement: '.our-location',
    triggerHook: 1,
    duration: '100%',
  })
  .setTween(locationsTween)
  window.controller.addScene(locations);

  // Parallax location images
  var pictureTween = TweenMax.fromTo('.our-location__picture', 1, {y:'-15%', ease: Linear.easeNone}, {y:'-70%', ease: Linear.easeNone});
  var picture = new ScrollMagic.Scene({
    triggerElement: '.our-location',
    triggerHook: 1,
    duration: '175%',
  })
  .setTween(pictureTween)
  // .addIndicators()
  window.controller.addScene(picture);
};
var worklifeAnimations = function() {
  var titles = [
    // '.why-affinity__title',
    '.testimonials__title',
    '.grid-hiring__title'
  ];

  titles.forEach(function(selector) {
    var titleTween = TweenMax.from(selector, 1, {y: '100%', ease: Linear.easeNone}, 1);
    var scene = new ScrollMagic.Scene({
      triggerElement: selector,
      triggerHook: 1,
      duration: '100%',
    })
    .setTween(titleTween);
    window.controller.addScene(scene);
  });

  //Why affinity
  var whyTweenIn = TweenMax.fromTo('.why-affinity__title', 1, {y: height * 0.5, ease: Linear.easeNone}, {y: height * 0.15, ease: Linear.easeNone});
  var whyTweenIn2 = TweenMax.fromTo('.why-affinity__image', 1, {y:'100%', ease: Linear.easeNone}, {y:'0%', ease: Linear.easeNone});
  var whyTweenIn3 = TweenMax.fromTo('.why-affinity__text', 1, {y: height * 0.75, ease: Linear.easeNone}, {y: height * 0.15, ease: Linear.easeNone});

  var whyTweenOut = TweenMax.fromTo('.why-affinity__title', 1, {y: height * 0.15, ease: Linear.easeNone}, {y: height * -0.2, ease: Linear.easeNone});
  var whyTweenOut2 = TweenMax.fromTo('.why-affinity__image', 1, {y:'0%', ease: Linear.easeNone}, {y:'-100%', ease: Linear.easeNone});
  var whyTweenOut3 = TweenMax.fromTo('.why-affinity__text', 1, {y: height * 0.15, ease: Linear.easeNone}, {y: height * -0.2, ease: Linear.easeNone});

  var whyInTL = new TimelineMax();
  whyInTL.add([
    whyTweenIn,
    whyTweenIn2,
    whyTweenIn3
  ]);

  var whyOutTL = new TimelineMax();
  whyOutTL.add([
    whyTweenOut,
    whyTweenOut2,
    whyTweenOut3
  ]);

  var whyIn = new ScrollMagic.Scene({
    triggerElement: '.why-affinity',
    triggerHook: 1,
    duration: height * 0.8,
  })
  .setTween(whyInTL)
  // .addIndicators({name:'in'})

  var whyOut = new ScrollMagic.Scene({
    triggerElement: '.why-affinity',
    triggerHook: 0.2,
    duration: height * 0.8,
  })
  .setTween(whyOutTL)
  // .addIndicators({name:'out'})

  window.controller.addScene(whyIn);
  window.controller.addScene(whyOut);

  // var whyTween = TweenMax.staggerFrom('.why-affinity__text, .why-affinity__image', 1, {y:'80%', ease: Linear.easeNone}, 0.2);
  // var why = new ScrollMagic.Scene({
  //   triggerElement: '.why-affinity',
  //   triggerHook: 1,
  //   duration: '100%',
  // })
  // .setTween(whyTween)
  // window.controller.addScene(why);

  // Benefits
  var benefitsTween1 = TweenMax.staggerFrom(['.benefit--1', '.benefit--2','.benefits .grey-1'], 1, {y: height * 0.25, ease: Linear.easeNone}, 0.2);
  var benefits1 = new ScrollMagic.Scene({
    triggerElement: '.benefits',
    triggerHook: 1,
    duration: height * 0.666,
  })
  .setTween(benefitsTween1)
  window.controller.addScene(benefits1);

  var benefitsTween2 = TweenMax.staggerFrom(['.benefit--3', '.benefit--4','.benefit--5'], 1, {y: height * 0.25, ease: Linear.easeNone}, 0.2);
  var benefits2 = new ScrollMagic.Scene({
    triggerElement: '.benefits',
    triggerHook: 1,
    duration: height,
  })
  .setTween(benefitsTween2)

  window.controller.addScene(benefits2);

  var benefitsTween3 = TweenMax.staggerFrom(['.benefits .grey-2', '.benefit--6','.benefit--7'], 1, {y: height * 0.25, ease: Linear.easeNone}, 0.2);
  var benefits3 = new ScrollMagic.Scene({
    triggerElement: '.benefits',
    triggerHook: 1,
    duration: height * 1.333,
  })
  .setTween(benefitsTween3)

  window.controller.addScene(benefits3);

  // Testimonials
  var testimonialsTween = TweenMax.staggerFrom('.testimonials__slider', 1, {y:'20%', ease: Linear.easeNone}, 0.2);
  var testimonials = new ScrollMagic.Scene({
    triggerElement: '.testimonials',
    triggerHook: 1,
    duration: '100%',
  })
  .setTween(testimonialsTween)
  window.controller.addScene(testimonials);

  // Testimonials text
  var testimonials = new ScrollMagic.Scene({
    triggerElement: '.testimonials',
    triggerHook: 1,
    duration: height * 1.125,
  })
  .setTween(TweenMax.from('.testimonial__name, .testimonial__text', 1, {y: height * 0.333, ease: Linear.easeNone}));

  window.controller.addScene(testimonials);

  // Hiring steps
  var hiringTween = TweenMax.staggerFrom('.grid-hiring__slider', 1, {y:'50%', ease: Linear.easeNone}, 0.2);
  var hiring = new ScrollMagic.Scene({
    triggerElement: '.grid-hiring',
    triggerHook: 1,
    duration: '100%',
  })
  .setTween(hiringTween)
  window.controller.addScene(hiring);

  // Hiring steps text
  var hiring = new ScrollMagic.Scene({
    triggerElement: '.grid-hiring',
    triggerHook: 1,
    duration: height * 0.8,
  })
  .setTween(TweenMax.from('.hiring__text', 1, {y: height * 0.333, ease: Linear.easeNone}));

  window.controller.addScene(hiring);
};

var internshipsAnimations = function() {
  // var titles = [
  //   '.grid-opportunities .title'
  // ];
  //
  // titles.forEach(function(selector) {
  //   var titleTween = TweenMax.from(selector, 1, {y: height / 10, ease: Linear.easeNone}, 1);
  //   var scene = new ScrollMagic.Scene({
  //     triggerElement: selector,
  //     triggerHook: 1,
  //     duration: '100%',
  //   })
  //   .setTween(titleTween);
  //   window.controller.addScene(scene);
  // });

  // Opportunities
  var opportunitiesTweenIn = TweenMax.fromTo('.grid-opportunities .title', 1, {y:'100%', ease: Linear.easeNone},{y:'0%', ease: Linear.easeNone});
  var opportunitiesTweenIn2 = TweenMax.fromTo('.grid-opportunities .text', 1, {y:'200%', ease: Linear.easeNone}, {y:'0%', ease: Linear.easeNone});
  //var opportunitiesTweenIn3 = TweenMax.fromTo('.grid-opportunities__image-description', 1, {y:'200%', ease: Linear.easeNone}, {y:'50%', ease: Linear.easeNone});

  var opportunitiesTweenOut = TweenMax.fromTo('.grid-opportunities .title', 1, {y:'0%', ease: Linear.easeNone},{y:'-300%', ease: Linear.easeNone});
  var opportunitiesTweenOut2 = TweenMax.fromTo('.grid-opportunities .text', 1, {y:'0%', ease: Linear.easeNone}, {y:'-200%', ease: Linear.easeNone});
  //var opportunitiesTweenOut3 = TweenMax.fromTo('.grid-opportunities__image-description', 1, {y:'50%', ease: Linear.easeNone}, {y:'-200%', ease: Linear.easeNone});


  var opportunitiesInTL = new TimelineMax();
  opportunitiesInTL.add([
    opportunitiesTweenIn,
    opportunitiesTweenIn2,
    //opportunitiesTweenIn3
  ]);

  var opportunitiesOutTL = new TimelineMax();
  opportunitiesOutTL.add([
    opportunitiesTweenOut,
    opportunitiesTweenOut2,
    //opportunitiesTweenOut3
  ]);

  var opportunitiesIn = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities',
    triggerHook: 1,
    duration: height,
  })
  .setTween(opportunitiesInTL)
  // .addIndicators({name:'in'})

  var opportunitiesOut = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities',
    triggerHook: 0,
    duration: height,
  })
  .setTween(opportunitiesOutTL)
  // .addIndicators({name:'out'})

  window.controller.addScene(opportunitiesIn);
  window.controller.addScene(opportunitiesOut);


  // var opportunitiesTween = TweenMax.staggerFrom('.grid-opportunities .text, .grid-opportunities__image-description', 1, {y:height / 6, ease: Linear.easeNone}, 0.2);
  // var opportunities = new ScrollMagic.Scene({
  //   triggerElement: '.grid-opportunities',
  //   triggerHook: 1,
  //   duration: '100%',
  // })
  // .setTween(opportunitiesTween)
  // window.controller.addScene(opportunities);

  // Cases
  var casesTween = TweenMax.staggerFrom([
    '.grid-opportunities-cases:nth-child(1)',
    '.grid-opportunities-cases:nth-child(3)',
    '.grid-opportunities-cases:nth-child(2)'
  ], 1, {y:'20%', ease: Linear.easeNone}, 0.1);
  var cases = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-wrapper',
    triggerHook: 1,
    duration: '65%',
  })
  .setTween(casesTween)
  window.controller.addScene(cases);

  var casesTweenImg = TweenMax.staggerFromTo([
    '.grid-opportunities-cases:nth-child(1) picture',
    '.grid-opportunities-cases:nth-child(3) picture',
    '.grid-opportunities-cases:nth-child(2) picture'
  ], 1, {y:'-12%', ease: Linear.easeNone},{y:'0%', ease: Linear.easeNone}, 0.1);
  var casesImg = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-wrapper',
    triggerHook: .8,
    duration: '125%',
  })
  .setTween(casesTweenImg)
  window.controller.addScene(casesImg);
  //Content Cases
  var casesTweenContent = TweenMax.staggerFrom([
    '.grid-opportunities-cases:nth-child(1) .cases-content, .grid-opportunities-cases:nth-child(2) .cases-content, .grid-opportunities-cases:nth-child(3) .cases-content'
  ], 1, {y:'60', ease: Linear.easeNone}, 0.1);
  var casesContent = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-wrapper',
    triggerHook: 0.35,
    duration: '20%',
  })
  .setTween(casesTweenContent)
  window.controller.addScene(casesContent);

    var casesTweenExit = TweenMax.staggerTo([
    '.grid-opportunities-cases:nth-child(1)',
    '.grid-opportunities-cases:nth-child(3)',
    '.grid-opportunities-cases:nth-child(2)'
  ], 1, {y:'-20%', ease: Linear.easeNone}, 0.45);
  var casesExit = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-wrapper',
    triggerHook: 0,
    duration: '85%',
  })
  .setTween(casesTweenExit)
  window.controller.addScene(casesExit);

  var casesTweenContent2 = TweenMax.staggerFrom([
    '.grid-opportunities-cases:nth-child(4) .cases-content, .grid-opportunities-cases:nth-child(5) .cases-content, .grid-opportunities-cases:nth-child(6) .cases-content'
  ], 1, {y:'60', ease: Linear.easeNone}, 0.1);
  var casesContent2 = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-cases:nth-child(5)',
    triggerHook: 0.35,
    duration: '20%',
  })
  .setTween(casesTweenContent2)
  window.controller.addScene(casesContent2);


  var casesTween2 = TweenMax.staggerFrom([
    '.grid-opportunities-cases:nth-child(5)',
    '.grid-opportunities-cases:nth-child(4)',
    '.grid-opportunities-cases:nth-child(6)'
  ], 1, {y:'20%', ease: Linear.easeNone}, 0.25);
  var cases2 = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-cases:nth-child(4)',
    triggerHook: 1,
    duration: '75%',
  })
  .setTween(casesTween2)
  // .addIndicators()
  window.controller.addScene(cases2);

  var casesTweenImg2 = TweenMax.staggerFromTo([
    '.grid-opportunities-cases:nth-child(5) picture',
    '.grid-opportunities-cases:nth-child(4) picture',
    '.grid-opportunities-cases:nth-child(6) picture'
  ], 1, {y:'-12%', ease: Linear.easeNone},{y:'0%', ease: Linear.easeNone}, 0.1);
  var casesImg2 = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-cases:nth-child(2) h3',
    triggerHook: 0,
    duration: '125%',
  })
  .setTween(casesTweenImg2)
  window.controller.addScene(casesImg2);

  var casesTween2Exit = TweenMax.staggerTo([
    '.grid-opportunities-cases:nth-child(5)',
    '.grid-opportunities-cases:nth-child(4)',
    '.grid-opportunities-cases:nth-child(6)'
  ], 1, {y:'-20%', ease: Linear.easeNone}, 0.25);
  var cases2Exit = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities-cases:nth-child(4)',
    triggerHook: 0.1,
    duration: '95%',
  })
  .setTween(casesTween2Exit)
  // .addIndicators()
  window.controller.addScene(cases2Exit);

  // Opportunities picture
  var pictureTween = TweenMax.from('.grid-opportunities__image-description', 1, {y:'100%', ease: Linear.easeNone});
  var picture = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities',
    triggerHook: .5,
    duration: "75%",
  })
  .setTween(pictureTween)
  window.controller.addScene(picture);

  var pictureTweenImage = TweenMax.fromTo('.grid-opportunities .cases__picture', 1, {y:'-40%', ease: Linear.easeNone}, {y:'-20%', ease: Linear.easeNone});
  var pictureImg = new ScrollMagic.Scene({
    triggerElement: '.grid-opportunities',
    triggerHook: .5,
    duration: "100%",
  })
  .setTween(pictureTweenImage)
  window.controller.addScene(pictureImg);

};

var offersAnimations = function() {

  var titleTween = TweenMax.from('.openings-grid__title', 1, {y: '100%', ease: Linear.easeNone});
    var scene = new ScrollMagic.Scene({
      triggerElement: '.openings-wrapper',
      triggerHook: .65,
      duration: '50%',
    })
    .setTween(titleTween);
    var animated = false;
    scene.on('enter', function() {
      if (!animated) {
        TweenMax.from('.openings-grid__title span', 1, {width:'0%', ease:'wow', delay: 0.5});
        animated = true;
      }
    });
    window.controller.addScene(scene);

  // var offersTween = TweenMax.staggerFrom('.opening', 3, {y: '100%', ease: Linear.easeNone}, 1);
  //   var scene = new ScrollMagic.Scene({
  //     triggerElement: '.openings',
  //     triggerHook: .75,
  //     duration: '65%',
  //   })
  //   .setTween(offersTween);
  //   window.controller.addScene(scene);
  //
  // var filtersTween = TweenMax.from('.filters--teams', .4, {y: '100%', ease: Linear.easeNone}, 1);
  //   var scene = new ScrollMagic.Scene({
  //     triggerElement: '.openings__filters',
  //     triggerHook: 1,
  //     duration: '50%',
  //   })
  //   .setTween(filtersTween);
  //   window.controller.addScene(scene);
  //
  // var filtersTween2 = TweenMax.from('.filters--locations', .4, {y: '100%', ease: Linear.easeNone}, 1);
  //   var scene = new ScrollMagic.Scene({
  //     triggerElement: '.filters--teams',
  //     triggerHook: .5,
  //     duration: '65%',
  //   })
  //   .setTween(filtersTween2);
  //   window.controller.addScene(scene);
}

var coverAnimations = function() {
  var coverItems = [];
  var coverSpeeds = [
    '-100%',
    '-150%',
    '-50%',
    '-150%',
    '-100%',
    '-50%',
    '-50%',
    '-150%',
    '-100%',
    '-150%'
  ]
  $('.grid-cover .inner-image').append('<div class="item-cover item-cover--white"></div><div class="item-cover item-cover--grey"></div>');
  $('.grid-cover .grid__item--grey').append('<div class="item-cover item-cover--white"></div>');

  var tween = new TimelineMax({
    onComplete : function() {
      // enableScroll();
      //enter ExitAnimationsCover();

      var distance = height * -0.5;
      var duration = height * 0.6;

      //Text1
      var text_exit = TweenMax.to('.grid-cover__item-cover__pretitle', 1, {y: distance, delay: 0.0625});
      var text_exit_scene = new ScrollMagic.Scene({triggerElement: '.grid-cover', triggerHook: '0.05', duration: duration})
      .setTween( text_exit )
      //Text2
      var text_exit_1 = TweenMax.to('.grid-cover .main-title', 1, {y: distance, delay: 0.125});
      var text_exit_scene_1 = new ScrollMagic.Scene({triggerElement: '.grid-cover', triggerHook: '0.05', duration: duration})
      .setTween( text_exit_1 )
      //Text3
      var text_exit_2 = TweenMax.to('.grid-cover__item-cover__video', 1, {y: distance, delay: 0.25});
      var text_exit_scene_2 = new ScrollMagic.Scene({triggerElement: '.grid-cover', triggerHook: '0.05', duration: duration})
      .setTween( text_exit_2 )
      //Text4
      var text_exit_3 = TweenMax.to('.grid-cover__item-cover__line', 1, {y: distance, delay: 0.2});
      var text_exit_scene_3 = new ScrollMagic.Scene({triggerElement: '.grid-cover', triggerHook: '0.05', duration: duration})
      .setTween( text_exit_3 )
      //Text5
      var text_exit_4 = TweenMax.to('.grid-cover__item-cover__text-content', 1, {y: distance, delay: 0.33});
      var text_exit_scene_4 = new ScrollMagic.Scene({triggerElement: '.grid-cover', triggerHook: '0.05', duration: duration})
      .setTween( text_exit_4 )

      window.controller.addScene([
        text_exit_scene,
        text_exit_scene_1,
        text_exit_scene_2,
        text_exit_scene_3,
        text_exit_scene_4
      ]);
      //Show btn
      $('.grid-cover__item-cover__viewmore').addClass('complete-show');

      // Parallax images
      var pictureTween = TweenMax.to('.grid-cover .featured__picture', 1, {y:'-30%', ease: Linear.easeNone});
      var picture = new ScrollMagic.Scene({
        triggerElement: '.grid-cover',
        triggerHook: 0,
        duration: '50%',
      })
      .setTween(pictureTween)
      // .addIndicators()
      window.controller.addScene(picture);

      // Exit cover
      coverItems.forEach(function(element, i) {
        $(element).removeAttr('style');
        var speed = coverSpeeds[i] || '-100%';
        var itemTween = TweenMax.to(element, 1, {y: speed, ease: Linear.easeNone}, 1);
        var scene = new ScrollMagic.Scene({
          triggerElement: '.grid-cover',
          triggerHook: 0,
          duration: '100%',
        })
        // .addIndicators({name: 'cover items ' + selector, colorEnd: '#f0f'})
        .setTween(itemTween);
        window.controller.addScene(scene);
      });
    }
  });
  var delay = 0.5;
  $('.grid-cover__item-cover__image').not('.grid-cover__item-cover__image--mask').each(function(index, el) {

    var $el = $(el);
    coverItems.push(el);
    delay = delay + index * 0.1;

    var sequence = [
        TweenMax.from($el, 1, {y: 400, ease: wow, delay: 1}),
        TweenMax.to($el.find('.item-cover--white'), 0.6, {x: '-100%', ease: wow, delay: 1.5}),
        TweenMax.to($el.find('.item-cover--grey'), 0.5, {x: '-100%', ease: wow, delay: 1.7})
      ];
    tween.add(sequence, delay);
  });
  $('.grid-cover .grid__item--grey:visible').each(function(index, el) {

    var $el = $(el);
    coverItems.push(el);
    delay = delay + index * 0.1;

    var sequence = [
        TweenMax.from($el, 1, {y: 400, ease: wow, delay: 1}),
        TweenMax.to($el.find('.item-cover--white'), 0.6, {x: '-100%', ease: wow, delay: 1.5})
      ];
    tween.add(sequence, delay);
  });
};
if (window.mediaQuery('desktop')) {
  animateSquares();
  aboutAnimations();
  worklifeAnimations();
  internshipsAnimations();
  offersAnimations();
}

/*var noScroll = function(e) {
  if (e) {
    e.preventDefault();
  }
  window.scrollTo(0, 0);
  return false;
};
var disableScroll = function() {
  // add listener to disable scroll
  window.addEventListener('scroll', noScroll);
};
*/
// var enableScroll = function() {
//
//   /*$.getScript('libs/SmoothScroll.min.js', function() {*/
//   $.getScript(Drupal.settings.theme + '/js/SmoothScroll.min.js', function() {
//     window.SmoothScroll({stepSize: 100});
//     /*// Remove listener to disable scroll
//     window.removeEventListener('scroll', noScroll);*/
//   });
// };
/*if (window.mediaQuery('desktop')) {
  disableScroll();
}*/

$(document).ready(function() {
  if (window.mediaQuery('desktop')) {
    coverAnimations();
    /*setTimeout(function() {
      noScroll();
    }, 0);*/
  }
});
/*window.onbeforeunload = function () {
  if (window.mediaQuery('desktop')) {
    setTimeout(function() {
      noScroll();
    }, 200);
  }
};*/
