<?php

/**
 * @file
 * testimonials_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function testimonials_feature_node_info() {
  $items = array(
    'testimonials' => array(
      'name' => t('Testimonials'),
      'base' => 'node_content',
      'description' => t('Worklife: Testimonials'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
