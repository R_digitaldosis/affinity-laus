<?php

/**
 * @file
 * cases_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function cases_feature_node_info() {
  $items = array(
    'cases' => array(
      'name' => t('Cases'),
      'base' => 'node_content',
      'description' => t('Internship: Cases'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
