<?php

/**
 * @file
 * offices_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function offices_feature_node_info() {
  $items = array(
    'offices' => array(
      'name' => t('Offices'),
      'base' => 'node_content',
      'description' => t('About: offices'),
      'has_title' => '1',
      'title_label' => t('Location'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
