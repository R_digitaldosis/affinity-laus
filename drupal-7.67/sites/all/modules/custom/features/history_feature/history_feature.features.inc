<?php

/**
 * @file
 * history_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function history_feature_node_info() {
  $items = array(
    'history' => array(
      'name' => t('History'),
      'base' => 'node_content',
      'description' => t('About: history'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
