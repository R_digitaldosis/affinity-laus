<?php

/**
 * @file
 * history_feature.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function history_feature_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Affinity Foundation is born. Our mission is to promote the role of pet animals and the benefits they have on people.\',
            \'summary\' => \'\',
            \'format\' => \'plain_text\',
            \'safe_value\' => "<p>Affinity Foundation is born. Our mission is to promote the role of pet animals and the benefits they have on people.</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => NULL,
      \'field_image\' => array(),
      \'field_year\' => array(
        \'und\' => array(
          array(
            \'value\' => \'1987\',
            \'format\' => NULL,
            \'safe_value\' => \'1987\',
          ),
        ),
      ),
      \'files\' => array(),
      \'language\' => \'en\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'admin\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'1987\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'history\',
      \'uid\' => \'1\',
      \'uuid\' => \'05276353-02ea-4b07-a2ef-53ea3eb28ece\',
      \'vid\' => NULL,
      \'vuuid\' => \'7c3c886f-f996-4667-9190-5f66df10f38d\',
    ),
  (object) array(
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Affinity is born in Barcelona\',
            \'summary\' => \'\',
            \'format\' => \'plain_text\',
            \'safe_value\' => "<p>Affinity is born in Barcelona</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => NULL,
      \'field_image\' => array(),
      \'field_year\' => array(
        \'und\' => array(
          array(
            \'value\' => \'1963\',
            \'format\' => NULL,
            \'safe_value\' => \'1963\',
          ),
        ),
      ),
      \'files\' => array(),
      \'language\' => \'en\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'admin\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'1963\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'history\',
      \'uid\' => \'1\',
      \'uuid\' => \'7d324bc7-90d7-4d4f-bbd2-d41dfb3b6180\',
      \'vid\' => NULL,
      \'vuuid\' => \'e5f680e5-a7df-437d-9847-562e06602ae1\',
    ),
  (object) array(
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => \'Launch of the first campaign "He never would"\',
            \'summary\' => \'\',
            \'format\' => \'plain_text\',
            \'safe_value\' => "<p>Launch of the first campaign &quot;He never would&quot;</p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => NULL,
      \'field_image\' => array(),
      \'field_year\' => array(
        \'und\' => array(
          array(
            \'value\' => \'1988\',
            \'format\' => NULL,
            \'safe_value\' => \'1988\',
          ),
        ),
      ),
      \'files\' => array(),
      \'language\' => \'en\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'admin\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => \'1\',
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'1988\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'history\',
      \'uid\' => \'1\',
      \'uuid\' => \'c04c2094-ccdf-4258-820a-e3f7ef138e12\',
      \'vid\' => NULL,
      \'vuuid\' => \'c59dd744-020e-488e-b487-7f4317236f54\',
    ),
)',
);
  return $node_export;
}
