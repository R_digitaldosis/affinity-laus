<?php

/**
 * @file
 * hiring_steps_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function hiring_steps_feature_node_info() {
  $items = array(
    'hiring_steps' => array(
      'name' => t('Hiring Steps'),
      'base' => 'node_content',
      'description' => t('Worklife: Hiring steps'),
      'has_title' => '1',
      'title_label' => t('Number'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
