<?php

/**
 * @file
 * hiring_steps_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hiring_steps_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-hiring_steps-field_steps_text'.
  $field_instances['node-hiring_steps-field_steps_text'] = array(
    'bundle' => 'hiring_steps',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_steps_text',
    'label' => 'Text',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Text');

  return $field_instances;
}
