<?php

/**
 * @file
 * benefits_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function benefits_feature_node_info() {
  $items = array(
    'benefits' => array(
      'name' => t('Benefits'),
      'base' => 'node_content',
      'description' => t('Worklife: Benefits'),
      'has_title' => '1',
      'title_label' => t('Benefit'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
