<?php

/**
 * @file
 * values_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function values_feature_node_info() {
  $items = array(
    'values' => array(
      'name' => t('Values'),
      'base' => 'node_content',
      'description' => t('About: values'),
      'has_title' => '1',
      'title_label' => t('Value'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
