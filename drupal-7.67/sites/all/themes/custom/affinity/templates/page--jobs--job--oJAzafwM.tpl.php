<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="menu js-menu">
  <header class="header menu__header">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

    <button class="header__burger hamburger hamburger--squeeze is-active js-close-menu" type="button">
      <span class="hamburger-box"> <span class="hamburger-inner"></span> </span>
    </button>
  </header>
  <nav class="menu__nav">
    <?php $mobile_menu = menu_navigation_links('menu-mobile-menu');
    print theme('links__system_menu_mobile_menu', array('links' => $mobile_menu) ); ?>
  </nav>
  <?php
    if (!isset($node)) {
      global $node;
    }
    $jobId = arg(2);

    $langList = language_list();
    global $language;
    global $base_url;
    $currentLang = $language->language;
  ?>
  <div class="menu__footer">
    <nav class="menu__languages">
      <?php foreach ($langList as $lang): ?>
      <?php

      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language == $currentLang): ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader is-active">
          <?php echo $lang->language; ?>
        </a>
      <?php else: ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader">
          <?php echo $lang->language; ?>
        </a>
      <?php endif; ?>
     <?php endforeach; ?>
    </nav>
    <?php
	    $options = array('absolute' => TRUE);
	    $nid = 10; //cambiar por ID Legal Page
	    $url_legalPage = url('node/' . $nid, $options);
    ?>
    <div class="menu__copy">
      Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>. <br /><?php print t('All rights reserved.'); ?> <a href="<?php echo $url_legalPage;?>" class="js-exit-loader">Legal notice</a>
    </div>
  </div>
</div>
<div id="page">
<div class="header-wrapper js-header-wrapper">
<header class="header js-header" role="banner">

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

  <nav class="header__externals">
    <?php
      $links = menu_navigation_links('menu-external');
      foreach ($links as $link): ?>
      <a href="<?php echo $link['href']; ?>" target="_blank" class="header__external">
        <?php echo $link['title']; ?>
      </a>
    <?php endforeach; ?>
  </nav>
  <!-- languages selector -->
  <div class="header__languages language-selector js-language-selector">
    <div class="language-selector__selected" data-selected="<?php echo $currentLang; ?>">
      <?php echo strtoupper($currentLang); ?>
    </div>
    <ul class="language-selector__languages">
      <?php foreach ($langList as $lang): ?>
      <?php
      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language != $currentLang): ?>
        <li class="language-selector__language" data-language="<?php echo $lang->language; ?>">
          <a class="language-selector__link js-exit-loader" href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>">
            <?php echo strtoupper($lang->language); ?>
          </a>
        </li>
      <?php endif; ?>
     <?php endforeach; ?>
    </ul>
  </div>

  <nav class="header__nav">
  <?php $main_menu = menu_navigation_links('main-menu');
  print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
  </nav>
  <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>

  <?php //print render($page['header']); ?>

</header>
</div>
<?php
  $job = d26jobs_get_requisition($jobId);
?>
<section class="grid grid-cover grid-cover--offer">
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-1" data-column="7" data-row="1"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-2" data-column="2" data-row="2"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-3" data-column="6" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-4" data-column="1" data-row="5"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-desktop grey-5" data-column="8" data-row="3"></div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title">
          <?php echo t('JOIN US!'); ?>
        </p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title">
        <h1 class="main-title">
          Marketing Procurement Manager
        </h1>
        <h2 class="main-subtitle">
          Spain (L’Hospitalet de Llobregat)
        </h2>
    </div>
    <a href="#" class="grid__item cta cta--reversed js-scroll-to is-mobile" data-scroll=".offer__footer">
      <?php echo t('Apply'); ?>
    </a>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to complete-show" data-scroll=".offer__footer" data-column="7" data-row="3">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('Apply'); ?>
        </div>
    </div>
</section>
<section class="offer">
  <div class="offer__content">
    <strong>What do we offer?</strong><br>We offer a Procurement Manager position (indirect purchases) managing Marketing purchase, advertising expenses &amp; promotional materials in the Corporate Marketing Services Department. It is a transversal department with a wide consumer vision from the organization and many opportunities to develop your professional career.<br><p>&nbsp;</p><strong>Affinity Benefits</strong><br>Little things make a big difference. That’s why we want you to have the best of our care so you can start leaving your footprint. In Affinity you’ll have plenty of benefits:<br><ul><li>Healthy breakfasts</li><li>Fruit all day</li><li>Gym</li><li>Flexible compensation</li><li>Meal tickets</li><li>Pension plan –after 2 years in the Company-</li><li>Fun &amp; celebrations</li><li>Pets are welcomed in the office –all our offices are pet friendly-</li></ul><p>&nbsp;</p><p>&nbsp;<strong>What will you do?</strong></p>Affinity wants to change the way Indirect Purchases are made in the company in order to have more control and cost efficiencies in all areas. Therefore we need a Procurement Manager that will:<br><ul><li>Design the action plan to capture incremental savings although procurement should focus on delivering value (right balance between cost-service-quality)</li><li>Implement a new purchasing platform and processes</li><li>Design the new way of working through this platform and train all stakeholders in the company (drive the change management process)</li><li>Negotiate contracts, lead pitches and improve price lists and quotations as a day to day supporting our Business Units and other stakeholders.</li><li>Supervise the writing of contracts together with the Legal Department.</li><li>Search for the best suppliers for the Business</li><li>Homologate suppliers and authorize their set up in the system</li><li>Negotiate improved conditions for current suppliers.</li><li>Control savings of categories managed and follow up on volume discounts to be given by suppliers.</li><li>Report cost savings and efficiencies periodically.</li></ul><p>&nbsp;&nbsp;</p><strong>What are we looking for?</strong><br><ul><li>We are looking for a person with at least 3 years of experience in marketing purchase, advertising expenses and promotional materials. With leadership skills and desire to face new challenges and take responsibilities.</li></ul><p><span style="text-decoration: underline;">Knowledge</span></p><ul><li>Holds a Business Degree</li><li>Excellent command of Microsoft Office (Word, Excel and PowerPoint)</li><li>Fluent written and verbal communication in English and Spanish, other languages such as French, Italian or Portuguese will be highly valued</li></ul><p>&nbsp;<span style="text-decoration: underline;">Attitude</span></p><ul><li>Passionate for his/ her job</li><li>Highly self-motivated, proactive and energetic</li><li>Willing to implement change</li><li>Desire to improve status quo and make a positive impact on the organization</li><li>Likes to take control</li></ul><p>&nbsp;<span style="text-decoration: underline;">Skills</span></p><ul><li>Strong negotiation and interpersonal skills</li><li>Able to make decisions and take leadership in his/her area</li><li>High technical and analytical skills</li><li>Ability to work in transversal teams with different people from departments such as marketing and finance but also agencies and other suppliers.</li><li>Self driven, autonomous…</li><li>Strategic business vision</li></ul>
  </div>
  <div class="offer__footer">
    <h2 class="offer__title">
      <?php echo t('Submit your application,');?>
    </h2>
    <div class="offer__text">
      <p>
        <?php echo t("we are ready to meet you!"); ?>
      </p>
    </div>
    <a href="#" class="offer__apply">
      <?php echo t('Apply'); ?>
    </a>
  </div>
</section>

<?php print render($page['footer']); ?>



<?php print render($page['bottom']);?>
