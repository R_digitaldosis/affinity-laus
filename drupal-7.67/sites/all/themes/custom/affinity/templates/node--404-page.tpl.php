<?php
  global $base_path;
  $theme = $base_path . 'sites/all/themes/custom/affinity';
?>
<section class="grid grid-cover grid-cover--404">
  <div
    class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey"
    data-column="7"
    data-row="1"
  ></div>
  <div
    class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey"
    data-column="2"
    data-row="2"
  ></div>
  <div
    class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey"
    data-column="6"
    data-row="4"
  ></div>
  <div
    class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey"
    data-column="1"
    data-row="5"
  ></div>
  <div
    class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-desktop"
    data-column="7"
    data-row="3"
  ></div>
  <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
    <p class="pre-main-title">
      <?php print render($content['field_prefix_404']);?>
    </p>
  </div>
  <div class="grid__item grid-cover__item-cover__title grid-cover__item-cover__title--title-error">
    <h1 class="main-title">
      <?php print render($content['field_title_404']);?>
    </h1>
  </div>
  <div class="grid__item grid-cover__text text">
    <p>
      <?php print render($content['field_text_404']);?>
    </p>
    <a href="<?php echo url('<front>');?>" class="cta js-exit-loader">
      <?php echo t('Go to Homepage'); ?>
    </a>
  </div>
  <div class="grid__item grid-cover__image">
    <img src="<?php echo $theme;?>/img/404/404.png" alt="" />
  </div>
</section>
<script type="text/javascript">
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({
   'event': 'adv_event',
   'event_custom': 'waf_error:404'
  });
</script>
