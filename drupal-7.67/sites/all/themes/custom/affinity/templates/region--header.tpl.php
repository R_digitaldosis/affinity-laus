
<?php if ($content): ?>
    <!-- languages selector -->
    <div class="header__languages language-selector js-language-selector">
      <div class="language-selector__selected" data-selected="es">ES</div>
      <!-- <ul class="language-selector__languages">
        <li class="language-selector__language" data-language="en">EN</li>
        <li class="language-selector__language" data-language="fr">FR</li>
        <li class="language-selector__language" data-language="it">IT</li>
        <li class="language-selector__language" data-language="pt">PT</li>
      </ul> -->
      <?php
      $block = module_invoke('locale', 'block_view', 'language');
      // print render($block);
       ?>
    </div>

    <nav class="header__nav">
    <?php $main_menu = menu_navigation_links('main-menu');
    print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
    </nav>
    <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>
<?php endif; ?>
