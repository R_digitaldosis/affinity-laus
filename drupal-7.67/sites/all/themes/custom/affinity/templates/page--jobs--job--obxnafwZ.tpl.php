<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="menu js-menu">
  <header class="header menu__header">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

    <button class="header__burger hamburger hamburger--squeeze is-active js-close-menu" type="button">
      <span class="hamburger-box"> <span class="hamburger-inner"></span> </span>
    </button>
  </header>
  <nav class="menu__nav">
    <?php $mobile_menu = menu_navigation_links('menu-mobile-menu');
    print theme('links__system_menu_mobile_menu', array('links' => $mobile_menu) ); ?>
  </nav>
  <?php
    if (!isset($node)) {
      global $node;
    }
    $jobId = arg(2);

    $langList = language_list();
    global $language;
    global $base_url;
    $currentLang = $language->language;
  ?>
  <div class="menu__footer">
    <nav class="menu__languages">
      <?php foreach ($langList as $lang): ?>
      <?php

      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language == $currentLang): ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader is-active">
          <?php echo $lang->language; ?>
        </a>
      <?php else: ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader">
          <?php echo $lang->language; ?>
        </a>
      <?php endif; ?>
     <?php endforeach; ?>
    </nav>
    <?php
	    $options = array('absolute' => TRUE);
	    $nid = 10; //cambiar por ID Legal Page
	    $url_legalPage = url('node/' . $nid, $options);
    ?>
    <div class="menu__copy">
      Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>. <br /><?php print t('All rights reserved.'); ?> <a href="<?php echo $url_legalPage;?>" class="js-exit-loader">Legal notice</a>
    </div>
  </div>
</div>
<div id="page">
<div class="header-wrapper js-header-wrapper">
<header class="header js-header" role="banner">

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

  <nav class="header__externals">
    <?php
      $links = menu_navigation_links('menu-external');
      foreach ($links as $link): ?>
      <a href="<?php echo $link['href']; ?>" target="_blank" class="header__external">
        <?php echo $link['title']; ?>
      </a>
    <?php endforeach; ?>
  </nav>
  <!-- languages selector -->
  <div class="header__languages language-selector js-language-selector">
    <div class="language-selector__selected" data-selected="<?php echo $currentLang; ?>">
      <?php echo strtoupper($currentLang); ?>
    </div>
    <ul class="language-selector__languages">
      <?php foreach ($langList as $lang): ?>
      <?php
      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language != $currentLang): ?>
        <li class="language-selector__language" data-language="<?php echo $lang->language; ?>">
          <a class="language-selector__link js-exit-loader" href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>">
            <?php echo strtoupper($lang->language); ?>
          </a>
        </li>
      <?php endif; ?>
     <?php endforeach; ?>
    </ul>
  </div>

  <nav class="header__nav">
  <?php $main_menu = menu_navigation_links('main-menu');
  print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
  </nav>
  <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>

  <?php //print render($page['header']); ?>

</header>
</div>
<?php
  $job = d26jobs_get_requisition($jobId);
?>
<section class="grid grid-cover grid-cover--offer">
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-1" data-column="7" data-row="1"></div>
    <div class="grid__item grid__item--grey grids-cover__item-cover grid-cover__item-cover--grey grey-2" data-column="2" data-row="2"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-3" data-column="6" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-4" data-column="1" data-row="5"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-desktop grey-5" data-column="8" data-row="3"></div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title">
          <?php echo t('JOIN US!'); ?>
        </p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title">
        <h1 class="main-title">
          Operations IT Business Partner
        </h1>
        <h2 class="main-subtitle">
          Spain (L’Hospitalet de Llobregat)
        </h2>
    </div>
    <a href="#" class="grid__item cta cta--reversed js-scroll-to is-mobile" data-scroll=".offer__footer">
      <?php echo t('Apply'); ?>
    </a>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to complete-show" data-scroll=".offer__footer" data-column="7" data-row="3">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('Apply'); ?>
        </div>
    </div>
</section>
<section class="offer">
  <div class="offer__content">
    <strong>What we offer</strong><br><span>We offer an Operations IT BP position to co-pilot with the Operations Managers the management and improvement of the operations processes in the most automatic and reliable way.</span><br><span style="text-decoration: underline;"><br>Location</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>Barcelona</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Travels to our factories in Barcelona, France and Italy</span></li></ul><strong><br></strong><strong>Affinity Benefits</strong><br><span>Little things make a big difference. That is why we want you to have the best of our care so you can start leaving your footprint. In Affinity, you will have plenty of benefits:</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>Healthy breakfasts</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Fruit all day</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Gym</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Flexible compensation</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Meal tickets</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Pension plan</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Fun &amp; celebrations</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Pets are welcomed in the office</span></li></ul><span><br><br><strong>What you will do</strong></span><br><span style="text-decoration: underline;">Main Objectives</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>To be a change agent in Operations accompany the company to reach the excellence in Purchasing, Manufacturing, Supply Chain and R&amp;D processes.</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Lead Operations area providing successful and automatic ways of collect and provide valuable information that helps the organization to take correct process decisions.</span></li></ul><span style="text-decoration: underline;">Major Areas of Responsibility</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>Manage, development and implementation of different Operations projects across the different areas (R&amp;D, Purchasing, Industrial &amp; Supply Chain)</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Build Operations IT infrastructure together with the IT team</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Manage Operations IT providers together with Operation key users</span></li></ul><span style="text-decoration: underline;">Specific Responsibilities of the Job</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>SAP ECC and EWM Operations Processes support, maintenance and control</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Supply Chain, Manufacturing, Purchasing &amp; R&amp;D specific applications support (APO, IBP, MES, Vendor Portal, Formulation, Laboratory, Nutrition Research Center</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Manage Operations IT suppliers (SAP and non-SAP)</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Guide and accompany the organization towards Industry 4.0</span></li></ul><strong><br><br>What we are looking for</strong><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>IT professional with a solid experience in Operations processes, including demand and supply planning, operations traceability and control, IT-OT integration (information technology –operations technology)</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Used to work and communicate in industrial environment</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Innovative and consistently keeping up-to-date with advancements of the field.</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Strong strategic and business thinking.</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Leadership, influencing &amp; communication skills</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Multi-country, multi-cultural</span></li></ul><span style="text-decoration: underline;">Knowledge, Skills and Abilities</span><br><ul><li style="-aw-headerfooter-type: footer-primary;"><span>IT Engineer</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>SAP APO, IBP</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>SAP ECC</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>SAP PI</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>EWM</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>MES</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>SQL</span></li><li style="-aw-headerfooter-type: footer-primary;"><span>Fluent in English</span></li></ul>
  </div>
  <div class="offer__footer">
    <h2 class="offer__title">
      <?php echo t('Submit your application,');?>
    </h2>
    <div class="offer__text">
      <p>
        <?php echo t("we are ready to meet you!"); ?>
      </p>
    </div>
    <a href="#" class="offer__apply">
      <?php echo t('Apply'); ?>
    </a>
  </div>
</section>

<?php print render($page['footer']); ?>



<?php print render($page['bottom']);?>
