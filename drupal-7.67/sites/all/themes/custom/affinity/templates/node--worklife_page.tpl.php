<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */

?>
<?php
  global $base_path;
  $theme = $base_path . 'sites/all/themes/custom/affinity';
  $cover = node_view(node_load(field_get_items('node', $node, 'field_worklife_cover')[0]['target_id']));
?>
<section class="grid grid-cover grid-cover--worklife">
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="7" data-row="1"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="2" data-row="2"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile" data-column="6" data-row="4"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile" data-column="1" data-row="5"></div>
  <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
      <div class="decoration-line"></div>
  </div>
  <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
    <p class="pre-main-title">
      <?php print render($cover['field_prefix_cover']);?>
    </p>
  </div>
  <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title grid-cover__item-cover__title--title-worklife">
      <h1 class="main-title">
        <?php print render($cover['field_title_cover']);?>
      </h1>
  </div>
  <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__text-content text">
      <p>
        <?php print render($cover['field_text_cover']);?>
      </p>
  </div>
  <?php
    $images_option = array("-a", "-b");
    for( $i=0;$i<5;$i++){
      $img_pos = rand(0, 1);
      $images[] = $images_option[$img_pos];
    }
  ?>
  <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="7" data-row="2">
      <div class="inner-image">
        <picture class="featured__picture">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-img-01@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/work-life/worklife-img-01.jpg, <?php echo $theme; ?>/img/work-life/worklife-img-01@2x.jpg 2x" type="image/jpeg" />
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-img-01@2x.webp" type="image/webp" />
          <source
            srcset="<?php echo $theme; ?>/img/work-life/worklife-img-01.webp, <?php echo $theme; ?>/img/work-life/worklife-img-01@2x.webp 2x"
            type="image/webp"
          />
          <img src="<?php echo $theme; ?>/img/work-life/worklife-img-01.jpg" alt="" />
        </picture>
      </div>
  </div>
  <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="8" data-row="4">
      <div class="inner-image">
        <picture class="featured__picture">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>@2x.webp" type="image/webp" />
          <source
              srcset="<?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/work-life/worklife-img-02<?php echo $images[0];?>.jpg" alt="" />
        </picture>
      </div>
  </div>
  <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to" data-scroll=".why-affinity" data-column="6" data-row="5">
      <div class="orange btn-roll-bottom__label">
        <?php echo t('See more'); ?>
      </div>
  </div>
</section>
<div class="why-affinity grid">
  <h2 class="why-affinity__title title">
    <?php print render($content['field_why_title']); ?>
  </h2>
  <div class="why-affinity__text text">
    <p>
      <img src="<?php echo $theme; ?>/img/work-life/leave-your-footprint.svg" alt="" class="why-affinity__image" />
      <?php print render($content['field_why_text']); ?>
    </p>
    <p>
      <?php print render($content['field_why_text_2']); ?>
    </p>
  </div>

  <div class="grid__item grid__item--grey grey-1"></div>
  <div class="grid__item grid__item--grey grey-2"></div>
</div>
<div class="benefits grid">
  <div class="benefits__list">
    <?php
    $benefits = field_get_items('node', $node, 'field_worklife_benefits');
    foreach ($benefits as $i => $benefit): ?>
      <?php
        $className = 'benefit--' . ($i + 1);
        $val = node_load($benefit['target_id']);
        $benefit = node_view($val);
      ?>
      <div class="benefit <?php echo $className; ?>">
        <img src="<?php echo $theme .'/img/work-life/0'.($i + 1); ?>-benefits-affinity.svg" alt="<?php echo $val->title; ?>" class="benefit__icon" />
        <div class="benefit__title orange">
          <?php echo $val->title; ?>
        </div>
        <div class="benefit__text text">
          <p>
            <?php print render($benefit['field_benefit_text']); ?>
          </p>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="benefits__disclaimer">
    <?php print render($content['field_disclaimer']); ?>
  </div>
  <div class="grid__item grid__item--grey grey-1"></div>
  <div class="grid__item grid__item--grey grey-2"></div>
  <div class="grid__item grid__item--grey grey-3"></div>
  <div class="grid__item grid__item--grey grey-4"></div>
  <div class="grid__item grid__item--grey grey-5"></div>
  <div class="grid__item grid__item--grey grey-6"></div>
  <div class="grid__item grid__item--grey grey-7"></div>
  <div class="grid__item grid__item--grey grey-8"></div>
</div>

<div class="testimonials grid">
  <div class="testimonials__title title">
    <?php print render($content['field_testimonials_title']); ?>
  </div>
  <div class="testimonials__slider swiper-container js-testimonials-slider">
    <div class="testimonials__arrow js-testimonials-slider-prev"></div>
    <div class="testimonials__arrow  testimonials__arrow--next js-testimonials-slider-next"></div>
    <div class="swiper-wrapper">
      <?php
      $testimonials = field_get_items('node', $node, 'field_worklife_testimonials');
      foreach ($testimonials as $i => $testimonial): ?>
        <?php
          $val = node_load($testimonial['target_id']);
          $image = field_get_items('node', $val, 'field_testimonial_image');
          $image = file_load($image[0]['fid']);
          $testimonial = node_view($val);

          $imageNum = str_pad($i + 1, 2, '0', STR_PAD_LEFT);
          $video = render($testimonial['field_testimonial_video']);
        ?>

        <div class="swiper-slide testimonials__slide testimonial">
          <div class="testimonial__right">
            <div class="testimonial__image js-open-lightbox" data-lightbox="video" href="<?php echo $video; ?>">
              <picture class="testimonial__picture">
                <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>@2x.jpg" type="image/jpeg" />
                <source
                  srcset="
                    <?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>.jpg,
                    <?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>@2x.jpg 2x
                  "
                  type="image/jpeg"
                />
                <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>@2x.webp" type="image/webp" />
                <source
                  srcset="
                    <?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>.webp,
                    <?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>@2x.webp 2x
                  "
                  type="image/webp"
                />
                <img src="<?php echo $theme; ?>/img/work-life/worklife-testimonial-<?php echo $imageNum; ?>.jpg" alt="" />
              </picture>
              <div class="testimonial__info">
                <h3 class="testimonial__title">
                  <?php echo $val->title; ?>
                </h3>
                <div class="testimonial__button video-button">
                  <div class="video-button__icon">
                    <img src="<?php echo $theme; ?>/img/play-white.svg" alt="" class="video-button__image" />
                  </div>
                  <span class="video-button__text orange">
                    <?php echo t('View testimonial'); ?>
                  </span>
                </div>
              </div>
            </div>
            <div class="testimonial__name orange">
              <?php print render($testimonial['field_testimonial_name']); ?> <span class="testimonial__position"><?php print render($testimonial['field_testimonial_position']); ?></span>
            </div>
            <div class="testimonial__text text">
              <p>
                <?php print render($testimonial['field_testimonial_text']); ?>
              </p>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
      <div class="swiper-slide swiper-slide--fill"></div>
    </div>
  </div>

  <div class="grid__item grid__item--grey grey-1"></div>
  <div class="grid__item grid__item--grey grey-2"></div>
  <div class="grid__item grid__item--grey grey-3"></div>
  <div class="grid__item grid__item--grey grey-4"></div>
  <div class="grid__item grid__item--grey grey-5"></div>
  <div class="grid__item grid__item--grey grey-6"></div>
</div>


<div class="grid grid-hiring">
  <div class="grid-hiring__title title">
    <?php print render($content['field_hiring_steps_title']); ?>
  </div>
  <div class="grid-hiring__slider swiper-container js-hiring-slider">
    <div class="grid-hiring__arrow grid-hiring__arrow--prev js-hiring-slider-prev"></div>
    <div class="grid-hiring__arrow grid-hiring__arrow--next js-hiring-slider-next"></div>
    <div class="swiper-wrapper">

      <?php
      $steps = field_get_items('node', $node, 'field_worklife_hiring_steps');
      foreach ($steps as $i => $step): ?>
        <?php
          $val = node_load($step['target_id']);
          $step = node_view($val);
          $num = $i + 1;
          ?>
        <div class="swiper-slide grid-hiring__slide">
          <div class="hiring hiring--<?php echo $num; ?>">
            <div class="hiring__number hiring--<?php echo $num; ?>__number">
              <p class="number">
                <?php echo $val->title; ?>
              </p>
            </div>
            <div class="hiring__line hiring--<?php echo $num; ?>__line">
              <div class="progress-hiring-line"></div>
              <div class="progress-hiring-circle"></div>
            </div>
            <div class="hiring__text hiring--<?php echo $num; ?>__text text">
              <p>
                <?php print render(field_view_field('node', $val, 'field_steps_text'))?>
              </p>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<section class="grid grid-find-your-job grid-find-your-job--worklife">
  <div class="grid__item grid__item--grey is-desktop" data-column="8" data-row="1"></div>
  <div class="grid__item grid__item--grey is-desktop" data-column="1" data-row="2"></div>
  <div class="grid__item grid-find-your-job__item grid-find-your-job__item-title">
    <h2 class="title">
      <?php echo t('Find your next<br />challenge at Affinity!'); ?>
    </h2>
  </div>
  <a href="<?php echo url('/jobs'); ?>" class="grid__item grid-find-your-job__item grid-find-your-job__item-cta btn-roll-bottom js-exit-loader">
    <div class="orange btn-roll-bottom__label">
      <?php echo t('See vacancies'); ?>
    </div>
  </a>
</section>
<?php
  drupal_add_js(array('theme' => $theme), 'setting');
  drupal_add_js($theme . '/js/animations.js', array(
    'scope' => 'footer',
    'weight' => 1
  ));
  drupal_add_js($theme . '/js/scroll.js', array(
    'scope' => 'footer',
    'weight' => 2
  ));
 ?>
