<?php
  global $base_path;
  $theme = $base_path . 'sites/all/themes/custom/affinity';
  $field = field_get_items('node', $node, 'field_cover');
  $cover_node = node_load($field[0]['target_id']);
?>
<section class="grid grid-cover grid-cover--internships">
    <div class="grid__item grid__item--grey grid-cover__item-internships" data-column="7" data-row="3"></div>
    <div class="grid__item grid__item--grey grid-cover__item-internships" data-column="2" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-internships" data-column="6" data-row="6"></div>
    <div class="grid__item grid__item--grey grid-cover__item-internships" data-column="1" data-row="7"></div>
    <div class="grid__item grid-cover__item-cover__line" data-column="2" data-row="7">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title"><?php echo render(field_view_field('node', $cover_node, 'field_prefix_cover')) ?></p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title grid-cover__item-cover__title--title-internships">
        <h1 class="main-title"><?php $title_cover = field_get_items('node', $cover_node, 'field_title_cover'); echo $title_cover[0]['value'] ?></h1>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__text-content text">
        <p><?php echo render(field_view_field('node', $cover_node, 'field_text_cover')) ?></p>
    </div>
    <?php
      $images_option = array("-a", "-b");
      for( $i=0;$i<5;$i++){
        $img_pos = rand(0, 1);
        $images[] = $images_option[$img_pos];
      }
    ?>
    <div class="grid__item grid-cover__item-cover__image grid-cover__item-cover__image--w2 is-desktop" data-column="7" data-row="1">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>@2x.webp" type="image/webp" />
            <source
                srcset="<?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>@2x.webp 2x"
                type="image/webp"
              />
              <img src="<?php echo $theme; ?>/img/internship/internships-img-01<?php echo $images[0];?>.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="8" data-row="3">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>.jpg, <?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>@2x.webp" type="image/webp" />
            <source
                srcset="<?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>.webp, <?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>@2x.webp 2x"
                type="image/webp"
              />
              <img src="<?php echo $theme; ?>/img/internship/internships-img-02<?php echo $images[1];?>.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="7" data-row="4">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-03@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/internship/internships-img-03.jpg, <?php echo $theme; ?>/img/internship/internships-img-03@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-03@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/internship/internships-img-03.webp, <?php echo $theme; ?>/img/internship/internships-img-03@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/internship/internships-img-03.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to" data-scroll=".grid-opportunities" data-column="6" data-row="5">
        <div class="orange btn-roll-bottom__label"><?php print t('See more');?></div>
    </div>
</section>
<section class="grid grid-opportunities">
    <div class="grid__item grid__item--grey grid-opportunities__item--grey grey-1" data-column="8" data-row="2"></div>
    <div class="grid__item grid__item--grey grid-opportunities__item--grey grey-2" data-column="1" data-row="4"></div>
    <div class="grid__item grid__item--grey grid__item--w2 grid__item--h2 grid-opportunities__item--grey grey-3" data-column="7" data-row="5"></div>
    <div class="grid__item grid__item--grey grid-opportunities__item--grey grey-4" data-column="3" data-row="9"></div>
    <div class="grid__item grid-opportunities__item grid-opportunities__image-description">
      <picture class="cases__picture is-mobile">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-04-mobile@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/internship/internships-img-04-mobile.jpg, <?php echo $theme; ?>/img/internship/internships-img-04-mobile@2x.jpg 2x" type="image/jpeg" />
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-04-mobile@2x.webp" type="image/webp" />
          <source
            srcset="<?php echo $theme; ?>/img/internship/internships-img-04-mobile.webp, <?php echo $theme; ?>/img/internship/internships-img-04-mobile@2x.webp 2x"
            type="image/webp"
          />
          <img src="<?php echo $theme; ?>/img/internship/internships-img-04-mobile.jpg" alt="" />
      </picture>
      <picture class="cases__picture is-desktop">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-04@2x.jpg" type="image/jpeg" />
        <source srcset="<?php echo $theme; ?>/img/internship/internships-img-04.jpg, <?php echo $theme; ?>/img/internship/internships-img-04@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internships-img-04@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/internship/internships-img-04.webp, <?php echo $theme; ?>/img/internship/internships-img-04@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/internship/internships-img-04.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid-opportunities__item grid-opportunities__title-description">
        <h2 class="title"><?php echo render(field_view_field('node', $node, 'field_opportunities_title')) ?></h2>
    </div>
    <div class="grid__item grid-opportunities__item grid-opportunities__text-description text">
        <?php $field = field_get_items('node', $node, 'field_opportunities_text');?>
        <?php echo $field[0]['value'];?>
        <?php // $output = field_view_value('node', $node, 'field_opportunities_text', $field[0]);
        //print drupal_render($output) ; ?>
    </div>
</section>
<div class="grid-opportunities-wrapper">
<?php
$field = field_get_items('node', $node, 'field_cases');
$index = 0;
foreach ($field as $i => $value) {
    $nombre = $value['entity']->title;
    $case_node = node_load($value['target_id']);
    $posicion = field_view_field('node', $case_node, 'field_case_position');
    $texto = field_view_field('node', $case_node, 'field_case_text');
    $image = field_get_items('node', $case_node, 'field_case_image');
    $image_url = file_create_url($image[0]['uri']);
    $image_mobile = field_get_items('node', $case_node, 'field_image_mobile');
    $image_url_mobile = file_create_url($image_mobile[0]['uri']);
    $uri_img = affinity_preprocess_node__internships_page($image_url,'jpg');
    $uri_img_webp = affinity_preprocess_node__internships_page($image_url,'webp');
    $uri_img_mobile = affinity_preprocess_node__internships_page($image_url_mobile,'jpg');
    $uri_img_mobile_webp = affinity_preprocess_node__internships_page($image_url_mobile,'webp');
    $is_desktop_class = ( $index > 2 ) ? ' is-desktop' : '';
    $index++;
    $imageNum = str_pad($i + 1, 2, '0', STR_PAD_LEFT);
?>
<section class="grid grid-opportunities-cases<?php echo $is_desktop_class?>">
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__image">
      <?php if ($i <= 2): ?>
        <picture class="cases__picture is-mobile">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile.jpg, <?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile@2x.jpg 2x" type="image/jpeg" />
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile@2x.webp" type="image/webp" />
          <source
            srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile.webp, <?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile@2x.webp 2x"
            type="image/webp"
          />
          <img src="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>-mobile.jpg" alt="" />
        </picture>
      <?php endif; ?>
      <picture class="cases__picture is-desktop">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>@2x.jpg" type="image/jpeg" />
        <source srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>.jpg, <?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>.webp, <?php echo $theme; ?>/img/internship/internship-fotointern-<?php echo $imageNum; ?>@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/internship/internships-testimonial-<?php echo $imageNum; ?>.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__title title title--case cases-content">
        <h3><?php echo $nombre;?></h3>
    </div>
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__subtitle cases-content">
        <p class="orange"><?php echo render($posicion) ?></p>
    </div>
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__text text text--case cases-content">
        <p><?php echo render($texto) ?></p>
    </div>
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__overlay"></div>
    <div class="grid__item grid-opportunities-cases__case grid-opportunities-cases__arrow-down"></div>
</section>
<?php
}
?>
</div>
<section class="grid grid-find-your-job grid-find-your-job--internships">
    <div class="grid__item grid__item--grey" data-column="1" data-row="1"></div>
    <div class="grid__item grid-find-your-job__item grid-find-your-job__item-title">
        <h2 class="title">
           <?php echo t('Find your next<br />challenge at Affinity!'); ?>
        </h2>
    </div>
    <a href="<?php echo url('/jobs'); ?>" class="grid__item grid-find-your-job__item grid-find-your-job__item-cta btn-roll-bottom js-exit-loader">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('See vacancies'); ?>
        </div>
    </a>
</section>
<?php
  drupal_add_js(array('theme' => $theme), 'setting');
  drupal_add_js($theme . '/js/animations.js', array(
    'scope' => 'footer',
    'weight' => 1
  ));
  drupal_add_js($theme . '/js/scroll.js', array(
    'scope' => 'footer',
    'weight' => 2
  ));
 ?>
