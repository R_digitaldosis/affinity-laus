<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
 global $base_path;
 $theme = $base_path . 'sites/all/themes/custom/affinity';

?>
<?php
  $cover = node_view(node_load(field_get_items('node', $node, 'field_about_cover')[0]['target_id']));
?>
<section class="grid grid-cover grid-cover--about">
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="7" data-row="1"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="2" data-row="2"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile" data-column="6" data-row="4"></div>
  <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile" data-column="1" data-row="5"></div>
  <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
      <div class="decoration-line"></div>
  </div>
  <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
      <p class="pre-main-title">
      <?php print render($cover['field_prefix_cover']);?>
    </p>
  </div>
  <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title grid-cover__item-cover__title--title-about">
      <h1 class="main-title">
        <?php print render($cover['field_title_cover']);?>
      </h1>
  </div>
  <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__text-content text">
      <p>
        <?php print render($cover['field_text_cover']);?>
      </p>
  </div>
  <?php
    $images_option = array("-a", "-b");
    for( $i=0;$i<5;$i++){
      $img_pos = rand(0, 1);
      $images[] = $images_option[$img_pos];
    }
  ?>
  <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="7" data-row="2">
      <div class="inner-image">
        <picture class="featured__picture">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />

          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>@2x.webp" type="image/webp" />
          <source
              srcset="<?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>@2x.webp 2x"
              type="image/webp"
          />
          <img src="<?php echo $theme; ?>/img/about/about-img-01<?php echo $images[0];?>.jpg" alt="" />
        </picture>
      </div>
  </div>
  <div class="grid__item grid-cover__item-cover__image grid-cover__item-cover__image--w2 grid-cover__item-cover__image--h2 is-desktop" data-column="7" data-row="3">
    <div class="inner-image-mask"></div>
      <div class="inner-image">
        <picture class="featured__picture">
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-img-02@2x.jpg" type="image/jpeg" />
          <source srcset="<?php echo $theme; ?>/img/about/about-img-02.jpg, <?php echo $theme; ?>/img/about/about-img-02@2x.jpg 2x" type="image/jpeg" />
          <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-img-02@2x.webp" type="image/webp" />
          <source
            srcset="<?php echo $theme; ?>/img/about/about-img-02.webp, <?php echo $theme; ?>/img/about/about-img-02@2x.webp 2x"
            type="image/webp"
          />
          <img src="<?php echo $theme; ?>/img/about/about-img-02.jpg" alt="" />
        </picture>
      </div>
  </div>
  <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to" data-scroll=".our-mission" data-column="6" data-row="5">
      <div class="orange btn-roll-bottom__label">
        <?php echo t('See more'); ?>
      </div>
  </div>
</section>
<div class="our-mission-wrapper grid-wrapper">
  <div class="our-mission grid text">
    <h2 class="our-mission__title title">
      <?php print render($content['field_our_mission_title']); ?>
    </h2>
    <div class="our-mission__text text">
      <?php print render($content['field_our_mission_text1']); ?>
      <?php print render($content['field_our_mission_text2']); ?>
    </div>
    <div class="our-mission__text our-mission__text--desktop text text-1">
      <p>
        <?php print render($content['field_our_mission_text1']); ?>
      </p>
    </div>
    <div class="our-mission__text our-mission__text--right our-mission__text--desktop text text-2">
      <?php print render($content['field_our_mission_text2']); ?>
    </div>

    <div class="grid__item grid__item--grey grey-1"></div>
    <div class="grid__item grid__item--grey grey-2"></div>
    <div class="grid__item grid__item--grey grey-3"></div>
    <div class="grid__item grid__item--grey grey-4"></div>
    <div class="grid__item grid__item--grey grey-5"></div>
  </div>
</div>
<div class="our-history grid">
  <div class="our-history__title title">
    <?php print render($content['field_our_history_title']); ?>
  </div>
  <div class="our-history__slider swiper-container js-history-slider">
    <div class="our-history__arrow js-history-slider-prev"></div>
    <div class="our-history__arrow  our-history__arrow--next js-history-slider-next"></div>
    <div class="swiper-wrapper">
      <?php
      $histories = field_get_items('node', $node, 'field_our_history');
      foreach ($histories as $i => $history): ?>
        <?php
          $val = node_load($history['target_id']);
          $file = field_get_items('node', $val, 'field_benefit_icon');
          $history = node_view($val);

          $image = field_get_items('node', $val, 'field_image');
          $image = file_load($image[0]['fid']);

          $imageNum = str_pad($i + 1, 2, '0', STR_PAD_LEFT);
        ?>
        <div class="swiper-slide our-history__slide timeline">
          <div class="timeline__left">
            <div class="timeline__date">
              <?php print render($history['field_year']); ?>
            </div>
          </div>
          <div class="timeline__right">
            <div class="timeline__image">
              <picture class="timeline__picture">
                <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>@2x.jpg" type="image/jpeg" />
                <source
                  srcset="<?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>.jpg, <?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>@2x.jpg 2x"
                  type="image/jpeg"
                />
                <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>@2x.webp" type="image/webp" />
                <source
                  srcset="<?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>.webp, <?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>@2x.webp 2x"
                  type="image/webp"
                />
                <img src="<?php echo $theme; ?>/img/about/about-timeline-<?php echo $imageNum; ?>.jpg" alt="" />
              </picture>
            </div>
            <div class="timeline__title orange">
              <?php print render($history['field_year']); ?>
            </div>
            <div class="timeline__text text">
              <p>
                <?php print render($history['body']); ?>
              </p>
            </div>
          </div>
        </div>
      <?php endforeach; ?>


      <div class="swiper-slide swiper-slide--fill"></div>
    </div>
  </div>

  <div class="grid__item grid__item--grey grey-1"></div>
  <div class="grid__item grid__item--grey grey-2"></div>
  <div class="grid__item grid__item--grey grey-3"></div>
</div>
<div class="our-values-wrapper grid-wrapper">
  <div class="our-values grid text">
    <h2 class="our-values__title title">
      <?php print render($content['field_our_values_title']); ?>
    </h2>
    <?php $values = field_get_items('node', $node, 'field_our_values');?>
    <div class="our-values__values">
      <?php foreach ($values as $i => $value): ?>
        <?php
          $val = node_load($value['target_id']);
          $value = node_view($val);
        ?>
      <div class="our-values__value value" data-value="<?php echo $i + 1; ?>">
        <h3 class="value__title orange">
          <?php echo $val->title; ?>
        </h3>
        <div class="value__text text">
          <p>
            <?php print render($value['body']); ?>
          </p>
        </div>
      </div>
    <?php endforeach; ?>
    </div>
    <?php foreach ($values as $i => $value): ?>
      <?php
        $val = node_load($value['target_id']);
        $value = node_view($val);
      ?>
      <div class="our-values__value our-values__value--desktop value value--<?php echo $i + 1; ?>" data-value="<?php echo $i + 1; ?>">
        <h3 class="value__title orange">
          <?php echo $val->title; ?>
        </h3>
        <div class="value__text text">
          <p>
            <?php print render($value['body']); ?>
          </p>
        </div>
      </div>
    <?php endforeach; ?>

    <div class="grid__item grid__item--grey grey-1"></div>
    <div class="grid__item grid__item--grey grey-2"></div>
    <div class="grid__item grid__item--grey grey-3"></div>
    <div class="grid__item grid__item--grey grey-4"></div>
  </div>
</div>
<div class="our-location grid">
  <h2 class="our-location__title title">
    <?php print render($content['field_our_offices_title']); ?>
  </h2>
  <?php $offices = field_get_items('node', $node, 'field_our_offices');
  $loc = array('spain', 'france', 'italy', 'brazil');
  foreach ($offices as $i => $office): ?>
    <?php
      $val = node_load($office['target_id']);
      $office = node_view($val);

      $image = field_get_items('node', $val, 'field_offices_image');
      $image = file_load($image[0]['fid']);
      $location = $loc[$i];
      $imageNum = str_pad($i + 1, 2, '0', STR_PAD_LEFT);
    ?>
    <div class="our-location__image our-location__image--<?php echo $location; ?> js-location-hover" data-location="<?php echo $location; ?>">
      <picture class="our-location__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>@2x.jpg" type="image/jpeg" />
        <source
          srcset="<?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>.jpg, <?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>@2x.jpg 2x"
          type="image/jpeg"
        />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>.webp, <?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/about/about-ciudad-img-<?php echo $imageNum; ?>.jpg" alt="" />
      </picture>
    </div>

    <div class="our-location__box our-location__box--<?php echo $location; ?> js-location-hover" data-location="<?php echo $location; ?>">
      <h3 class="our-location__location orange">
        <?php echo $val->title; ?>
      </h3>
      <div class="our-location__text">
        <strong class="our-location__strong">
          <?php
          if ($i == 0) {
            echo t('Headquarters:');
          } else {
            echo t('Offices:');
          } ?>
        </strong>
        <span class="our-location__span">
          <?php print render($office['field_offices']); ?>
        </span>
      </div>
      <div class="our-location__text">
        <strong class="our-location__strong">
          <?php echo t('Production site:'); ?>
        </strong>
        <span class="our-location__span">
          <?php print render($office['field_manufacturing_site']); ?>
        </span>
      </div>
    </div>
  <?php endforeach; ?>
  <div class="grid__item grid__item--grey grey-1"></div>
  <div class="grid__item grid__item--grey grey-2"></div>
  <div class="grid__item grid__item--grey grey-3"></div>
</div>
<section class="grid grid-find-your-job grid-find-your-job--about">
    <div class="grid__item grid__item--grey" data-column="1" data-row="1"></div>
    <div class="grid__item grid-find-your-job__item grid-find-your-job__item-title">
        <h2 class="title">
          <?php echo t('Find your next<br />challenge at Affinity!'); ?>
        </h2>
    </div>
    <a href="<?php echo url('/jobs'); ?>" class="grid__item grid-find-your-job__item grid-find-your-job__item-cta btn-roll-bottom js-exit-loader">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('See vacancies'); ?>
        </div>
    </a>
</section>
<?php
  drupal_add_js(array('theme' => $theme), 'setting');
  drupal_add_js($theme . '/js/animations.js', array(
    'scope' => 'footer',
    'weight' => 1
  ));
  drupal_add_js($theme . '/js/scroll.js', array(
    'scope' => 'footer',
    'weight' => 2
  ));
 ?>
