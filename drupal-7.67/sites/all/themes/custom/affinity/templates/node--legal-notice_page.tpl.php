<section class="grid grid-cover grid-cover--legal">
      <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="7" data-row="1"></div>
      <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="2" data-row="2"></div>
      <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="6" data-row="4"></div>
      <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="1" data-row="5"></div>
      <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-desktop" data-column="8" data-row="3"></div>
      <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="7">
          <div class="decoration-line"></div>
      </div>
      <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
          <p class="pre-main-title"><?php $field = field_get_items('node', $node, 'field_pre_title'); print $field[0]['value'];?></p>
      </div>
      <div class="grid__item grid-cover__item-cover__title grid-cover__item-cover__title--title-legal">
          <h1 class="main-title"><?php print $node->title;?></h1>
      </div>
</section>
<section class="grid grid-legal">
      <div class="grid__item grid-legal__item text">
            <?php $field = field_get_items('node', $node, 'field_content_legal');
            foreach ($field as $i => $value) {
                  print $value['value'];
            }; ?>
      </div>
</section>