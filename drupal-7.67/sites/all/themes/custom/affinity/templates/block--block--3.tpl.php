<section class="grid grid-find-your-job">
    <div class="grid__item grid__item--grey" data-column="1" data-row="1"></div>
    <div class="grid__item grid-find-your-job__item grid-find-your-job__item-title">
        <h2 class="title">Find your job<br>to work with us!</h2>
    </div>
    <div class="grid__item grid-find-your-job__item grid-find-your-job__item-cta btn-roll-bottom">
        <div class="orange btn-roll-bottom__label">Job Opportunities</div>
    </div>
</section>