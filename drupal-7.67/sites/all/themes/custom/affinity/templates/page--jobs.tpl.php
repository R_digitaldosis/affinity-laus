<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */

 global $base_path;
 $theme = $base_path . 'sites/all/themes/custom/affinity';
?>

<div class="menu js-menu">
  <header class="header menu__header">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

    <button class="header__burger hamburger hamburger--squeeze is-active js-close-menu" type="button">
      <span class="hamburger-box"> <span class="hamburger-inner"></span> </span>
    </button>
  </header>
  <nav class="menu__nav">
    <?php $mobile_menu = menu_navigation_links('menu-mobile-menu');
    print theme('links__system_menu_mobile_menu', array('links' => $mobile_menu) ); ?>
  </nav>
  <?php
    if (!isset($node)) {
      global $node;
    }
    $langList = language_list();
    global $language;
    global $base_url;
    $currentLang = $language->language;
  ?>
  <div class="menu__footer">
    <nav class="menu__languages">
      <?php foreach ($langList as $lang): ?>
      <?php
      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }

      if ($lang->language == $currentLang): ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs'); ?>" class="menu__language js-exit-loader is-active">
          <?php echo $lang->language; ?>
        </a>
      <?php else: ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs'); ?>" class="menu__language js-exit-loader">
          <?php echo $lang->language; ?>
        </a>
      <?php endif; ?>
     <?php endforeach; ?>
    </nav>
    <?php
      $options = array('absolute' => TRUE);
      $nid = 10; //cambiar por ID Legal Page
      $url_legalPage = url('node/' . $nid, $options);
    ?>
    <div class="menu__copy">
      Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>. <br /><?php print t('All rights reserved.'); ?> <a href="<?php echo $url_legalPage;?>" class="js-exit-loader">Legal notice</a>
    </div>
  </div>
</div>
<div id="page">
<div class="header-wrapper js-header-wrapper">
<header class="header js-header" role="banner">

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

  <!-- <div class="header__decorator"></div> -->
  <nav class="header__externals">
    <?php
      $links = menu_navigation_links('menu-external');
      foreach ($links as $link): ?>
      <a href="<?php echo $link['href']; ?>" target="_blank" class="header__external">
        <?php echo $link['title']; ?>
      </a>
    <?php endforeach; ?>
  </nav>
  <!-- languages selector -->
  <div class="header__languages language-selector js-language-selector">
    <div class="language-selector__selected" data-selected="<?php echo $currentLang; ?>">
      <?php echo strtoupper($currentLang); ?>
    </div>
    <ul class="language-selector__languages">
      <?php foreach ($langList as $lang): ?>
      <?php
      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language != $currentLang): ?>
        <li class="language-selector__language" data-language="<?php echo $lang->language; ?>">
          <a class="language-selector__link js-exit-loader" href="<?php echo url($base_url. '/'. $prefix . 'jobs'); ?>">
            <?php echo strtoupper($lang->language); ?>
          </a>
        </li>
      <?php endif; ?>
     <?php endforeach; ?>
    </ul>
  </div>

  <nav class="header__nav">
  <?php $main_menu = menu_navigation_links('main-menu');
  print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
  </nav>
  <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>

  <?php //print render($page['header']); ?>

</header>
</div>
<section class="grid grid-cover grid-cover--careers">
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="7" data-row="1"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="2" data-row="2"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="6" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="1" data-row="5"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey" data-column="8" data-row="9"></div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line is-desktop" data-column="2" data-row="4">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title">
          <?php echo t('WORK WITH US!'); ?>
        </p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title">
        <h1 class="main-title">
          <?php echo t('Find your career opportunity'); ?>
        </h1>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__text-content text">
        <p>
          <?php echo t('We like people who want to make a difference. We’re looking for people who relish challenges and want to carry on learning no matter what their speciality is. If you’re looking for a new challenge, we’re waiting for you! You’ll be involved in our projects from your first day and you’ll be able to track your own path.').'<br/><br/>'.t('Start leaving your footprint.');?>
        </p>
    </div>
    <?php
    $images_option = array("-a", "-b");
      for( $i=0;$i<2;$i++){
        $img_pos = rand(0, 1);
        $images[] = $images_option[$img_pos];
      }
    ?>
    <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="8" data-row="1">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/offers/offers-img-01<?php echo $images[0];?>.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__image is-desktop" data-column="6" data-row="2">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-02@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/offers/offers-img-02.jpg, <?php echo $theme; ?>/img/offers/offers-img-02@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-02@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/offers/offers-img-02.webp, <?php echo $theme; ?>/img/offers/offers-img-02@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/offers/offers-img-02.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__image grid-cover__item-cover__image--w2 grid-cover__item-cover__image--h2 is-desktop" data-column="7" data-row="3">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>.jpg, <?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>.webp, <?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/offers/offers-img-03<?php echo $images[1];?>.jpg" alt="" />
          </picture>
        </div>
    </div>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to" data-scroll=".openings-grid__title" data-column="6" data-row="5">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('See more'); ?>
        </div>
    </div>
</section>
<?php
  $jobs = d26jobs_get_requisitions();
  $categories = d26jobs_get_categories();
  $locations = d26jobs_get_locations();
?>
<div class="openings-wrapper">
  <div class="openings-grid grid">
    <h2 class="openings-grid__title title">
      <?php echo t('Current vacancies'); ?>
      <span></span>
    </h2>
    <button type="button" class="openings-grid__filters js-open-filters">
      <?php echo t('Filters'); ?>
    </button>
    <div class="grid__item grid__item--grey grey-1"></div>
  </div>
  <div class="openings">
    <div class="openings__list">
      <p class="openings__empty js-no-results">
        <?php echo t('No current vacancies meet the search criteria.'); ?>
      </p>
       <a href="/affinity/en/jobs/job/oAznafwq" class="opening js-opening js-exit-loader" data-category="Marketing" data-location="CaPHWfwm">
          <h3 class="opening__title">
            Senior Brand Manager (Spain)          </h3>
          <div class="opening__data">
            <span class="opening__info">
              Team: <strong>Marketing</strong>
            </span>
            <span class="opening__info">
              Location: <strong>Spain (L’Hospitalet de Llobregat)</strong>
            </span>
          </div>
        </a>
        <a href="/affinity/en/jobs/job/oqctafwZ" class="opening js-opening js-exit-loader" data-category="Human Resources" data-location="CaPHWfwm">
          <h3 class="opening__title">
            Employer Brand Manager          </h3>
          <div class="opening__data">
            <span class="opening__info">
              Team: <strong>Human Resources</strong>
            </span>
            <span class="opening__info">
              Location: <strong>Spain (L’Hospitalet de Llobregat)</strong>
            </span>
          </div>
        </a>
        <a href="/affinity/en/jobs/job/opynafwe" class="opening js-opening js-exit-loader" data-category="Marketing" data-location="CaPHWfwm">
          <h3 class="opening__title">
            Trainee - Advance Brand Management          </h3>
          <div class="opening__data">
            <span class="opening__info">
              Team: <strong>Marketing</strong>
            </span>
            <span class="opening__info">
              Location: <strong>Spain (L’Hospitalet de Llobregat)</strong>
            </span>
          </div>
        </a>
        <a href="/affinity/en/jobs/job/obxnafwZ" class="opening js-opening js-exit-loader" data-category="IT" data-location="CaPHWfwm">
          <h3 class="opening__title">
            Operations IT Business Partner          </h3>
          <div class="opening__data">
            <span class="opening__info">
              Team: <strong>IT</strong>
            </span>
            <span class="opening__info">
              Location: <strong>Spain (L’Hospitalet de Llobregat)</strong>
            </span>
          </div>
        </a>
        <a href="/affinity/en/jobs/job/oJAzafwM" class="opening js-opening js-exit-loader" data-category="Marketing" data-location="CaPHWfwm">
          <h3 class="opening__title">
            Marketing Procurement Manager          </h3>
          <div class="opening__data">
            <span class="opening__info">
              Team: <strong>Marketing</strong>
            </span>
            <span class="opening__info">
              Location: <strong>Spain (L’Hospitalet de Llobregat)</strong>
            </span>
          </div>
        </a>
    </div>
    <div class="openings__filters">
      <div class="filters filters--teams">
        <h3 class="filters__title title">
          <?php echo t('Teams'); ?>
        </h3>
        <div class="filters__list">
          <?php foreach ($categories as $i => $category): ?>
            <div class="filter">
              <div class="filter__checkbox">
                <input type="checkbox" class="filter__input js-filter" id="filter-team-<?php echo $i; ?>" data-category="<?php echo $category->name; ?>">
                <label for="filter-team-<?php echo $i; ?>" class="filter__label"></label>
              </div>
              <label for="filter-team-<?php echo $i; ?>" class="filter__text">
                <?php echo $category->name; ?>
              </label>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="filters filters--locations">
        <h3 class="filters__title title">
          <?php echo t('Locations'); ?>
        </h3>
        <div class="filters__list">
          <?php foreach ($locations as $i => $location): ?>
            <div class="filter">
              <div class="filter__checkbox">
                <input type="checkbox" class="filter__input js-filter" id="filter-location-<?php echo $i; ?>" data-location="<?php echo $location->eId; ?>">
                <label for="filter-location-<?php echo $i; ?>" class="filter__label"></label>
              </div>
              <label for="filter-location-<?php echo $i; ?>" class="filter__text">
                <?php echo $location->country.' ('.$location->city.')'; ?>
              </label>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="openings-popup js-filters-popup">
    <div class="openings-popup__header">
      <?php echo t('Filter vacancies'); ?>
      <span class="openings-popup__close js-close-filters">
        &times;
      </span>
    </div>
    <div class="openings-popup__body">
      <div class="filters">
        <h3 class="filters__title title">
          <?php echo t('Teams'); ?>
        </h3>
        <div class="filters__list">
          <?php foreach ($categories as $i => $category): ?>
            <div class="filter">
              <div class="filter__checkbox">
                <input type="checkbox" class="filter__input js-filter" id="filter-team-<?php echo $i; ?>-m" data-category="<?php echo $category->name; ?>">
                <label for="filter-team-<?php echo $i; ?>-m" class="filter__label"></label>
              </div>
              <label for="filter-team-<?php echo $i; ?>-m" class="filter__text">
                <?php echo $category->name; ?>
              </label>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="filters">
        <h3 class="filters__title title">
          <?php echo t('Locations'); ?>
        </h3>
        <div class="filters__list">
          <?php foreach ($locations as $i => $location): ?>
            <div class="filter">
              <div class="filter__checkbox">
                <input type="checkbox" class="filter__input js-filter" id="filter-location-<?php echo $i; ?>-m" data-location="<?php echo $location->eId; ?>">
                <label for="filter-location-<?php echo $i; ?>-m" class="filter__label"></label>
              </div>
              <label for="filter-location-<?php echo $i; ?>-m" class="filter__text">
                <?php echo $location->country.' ('.$location->city.')'; ?>
              </label>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
    <div class="openings-popup__footer">
      <button type="button" class="openings-popup__button js-apply-filters js-close-filters">
        <?php echo t('Apply filters'); ?>
      </button>
    </div>
  </div>
  <div class="openings-footer offer__footer">
    <h2 class="offer__title">
      <?php echo t('Join our talent community');?>
    </h2>
    <div class="offer__text">
      <p>
        <?php echo t("We'll get in touch when we have a good <br>opportunity for you."); ?>
      </p>
    </div>
    <a href="#" class="offer__apply">
      <?php echo t('Join now'); ?>
    </a>
  </div>
</div>
<?php print render($page['footer']); ?>



<?php print render($page['bottom']);?>
<?php
  drupal_add_js(array('theme' => $theme), 'setting');
  drupal_add_js($theme . '/js/animations.js', array(
    'scope' => 'footer',
    'weight' => 1
  ));
  drupal_add_js($theme . '/js/scroll.js', array(
    'scope' => 'footer',
    'weight' => 2
  ));
 ?>
