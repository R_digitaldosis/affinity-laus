<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="menu js-menu">
  <header class="header menu__header">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

    <button class="header__burger hamburger hamburger--squeeze is-active js-close-menu" type="button">
      <span class="hamburger-box"> <span class="hamburger-inner"></span> </span>
    </button>
  </header>
  <nav class="menu__nav">
    <?php $mobile_menu = menu_navigation_links('menu-mobile-menu');
    print theme('links__system_menu_mobile_menu', array('links' => $mobile_menu) ); ?>
  </nav>
  <?php
    if (!isset($node)) {
      global $node;
    }
    $langList = language_list();
    $langs = translation_node_get_translations($node->tnid);
    $currentLang = $node->language;
  ?>
  <div class="menu__footer">
    <nav class="menu__languages">
      <?php foreach ($langs as $lang): ?>
      <?php $prefix = $langList[$lang->language]->prefix . '/';
      if ($lang->language == $currentLang): ?>
        <a href="<?php echo url($prefix . drupal_get_path_alias('node/'.$lang->nid, $lang->language), array('absolute' => true, 'language' => $lang)); ?>" class="menu__language js-exit-loader is-active">
          <?php echo $lang->language; ?>
        </a>
      <?php else: ?>
        <a href="<?php echo url($prefix . drupal_get_path_alias('node/'.$lang->nid, $lang->language), array('absolute' => true, 'language' => $lang)); ?>" class="menu__language js-exit-loader">
          <?php echo $lang->language; ?>
        </a>
      <?php endif; ?>
     <?php endforeach; ?>
    </nav>
    <?php
	    $options = array('absolute' => TRUE);
	    $nid = 10; //cambiar por ID Legal Page
	    $url_legalPage = url('node/' . $nid, $options);
    ?>
    <div class="menu__copy">
      Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>. <br /><?php print t('All rights reserved.'); ?> <a href="<?php echo $url_legalPage;?>" class="js-exit-loader">Legal notice</a>
    </div>
  </div>
</div>
<div id="page">
<div class="header-wrapper js-header-wrapper">
  <header class="header js-header" role="banner">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
    <?php endif; ?>

    <nav class="header__externals">
      <?php
        $links = menu_navigation_links('menu-external');
        foreach ($links as $link): ?>
        <a href="<?php echo $link['href']; ?>" target="_blank" class="header__external">
          <?php echo $link['title']; ?>
        </a>
      <?php endforeach; ?>
    </nav>
    <!-- languages selector -->
    <div class="header__languages language-selector js-language-selector">
      <div class="language-selector__selected" data-selected="<?php echo $currentLang; ?>">
        <?php echo strtoupper($currentLang); ?>
      </div>
      <ul class="language-selector__languages">
        <?php foreach ($langs as $lang): ?>
        <?php
        $prefix = $langList[$lang->language]->prefix . '/';
        if ($lang->language != $currentLang): ?>
          <li class="language-selector__language" data-language="<?php echo $lang->language; ?>">
            <a class="language-selector__link js-exit-loader" href="<?php echo url($prefix . drupal_get_path_alias('node/'.$lang->nid, $lang->language), array('absolute' => true, 'language' => $lang)); ?>">
              <?php echo strtoupper($lang->language); ?>
            </a>
          </li>
        <?php endif; ?>
       <?php endforeach; ?>
      </ul>
    </div>

    <nav class="header__nav">
    <?php $main_menu = menu_navigation_links('main-menu');
    print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
    </nav>
    <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>

    <?php //print render($page['header']); ?>

  </header>
</div>

<?php print render($page['content']); ?>

<?php print render($page['footer']); ?>



<?php print render($page['bottom']);?>
