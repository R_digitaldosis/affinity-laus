<footer class="grid grid-footer">
    <div class="grid__item grid-footer__item grid-footer__legal">
        <?php
        global $language;
        $translations = translation_path_get_translations("node/240");
        ?>
        <p>Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>.</p>
        <p><span><?php print t('All rights reserved.')?></span> <?php print l(t('Legal notice'), $translations[$language->language]);?></p>
    </div>
    <div class="grid__item grid-footer__item grid-footer__logos grid-footer__logos--firstline">
        <?php global $base_path; ?>
        <?php print '<a href="https://www.fundacion-affinity.org/" target="_blank"><img src="' . $base_path . 'sites/all/themes/custom/affinity/img/affinity-fundacion.svg" />'; ?>
        <?php print '<a href="https://www.affinity-petcare.com/advance" target="_blank"><img src="' . $base_path . 'sites/all/themes/custom/affinity/img/affinity-advance.svg" />'; ?>
        <?php print '<a href="https://www.affinity-petcare.com/brekkies/es" target="_blank"><img src="' . $base_path . 'sites/all/themes/custom/affinity/img/affinity-brekkies.svg" />'; ?>
        <?php print '<a href="https://www.affinity-petcare.com/ultima" target="_blank"><img src="' . $base_path . 'sites/all/themes/custom/affinity/img/affinity-ultima.svg" />'; ?>
        <?php print '<a href="https://www.affinity-petcare.com/libra" target="_blank"><img src="' . $base_path . 'sites/all/themes/custom/affinity/img/affinity-libra.svg" />'; ?>
    </div>
    <div class="grid__item grid-footer__item grid-footer__social">
        <?php $options = array('absolute' => TRUE);
            $url_instagram = field_get_items('node', node_load(247), 'field_url');
            $url_linkedin = field_get_items('node', node_load(248), 'field_url');
        ?>
        <a href="<?php echo $url_linkedin[0]['value']?>" title="linkedin" target="_blank"><span class="linkedin"></span></a>
        <a href="<?php echo $url_instagram[0]['value']?>" title="instagram" target="_blank"><span class="instagram"></span></a>
    </div>
    <div class="grid__item grid-footer__item grid-footer__firma">
        <p><?php print t('Web Design Agency:')?></p>
        <p><a href="http://digitaldosis.com" target="_blank" title="Digital Dosis">Digital Dosis</a></p>
    </div>
</footer>
</div>

<div class="lightbox js-lightbox" data-lightbox="video">
  <div class="lightbox__overlay js-close-lightbox"></div>
  <div class="lightbox__body">
    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/CtQD46fBKWk?version=3&rel=0&controls=0&disablekb=1&fs=0&modestbranding=1&iv_load_policy=3" frameborder="0" allowfullscreen></iframe> -->
  </div>
</div>

<div class="cookies js-cookies">
  <div class="cookies__box">
    <p class="cookies__text">
      <?php
        $options = array('attributes' => array('class' => array('cookies__link')));
      ?>
      <?php print t('This website uses both first-party and third-party analytics, advertising and social media plugin cookies in order to enhance your browsing experience and display content adapted to your preferences. If you continue browsing, it will be considered that you have accepted their use. You can change your settings or obtain further information in our'); ?> <?php print l(t('cookies policy'), $translations[$language->language], $options);?>.
    </p>
    <button class="cookies__button js-accept-cookies" type="button">
      <?php print t('Close'); ?>
    </button>
  </div>
</div>

<div class="loader js-loader">
  <div class="grid grid-loader">
    <div class="grid-loader__col grid-loader__col--top" data-delay="1"></div>
    <div class="grid-loader__col grid-loader__col--top" data-delay="3"></div>
    <div class="grid-loader__col grid-loader__col--bottom" data-delay="1"></div>
    <div class="grid-loader__col grid-loader__col--top" data-delay="4"></div>
    <div class="grid-loader__col grid-loader__col--bottom" data-delay="2"></div>
    <div class="grid-loader__col grid-loader__col--bottom" data-delay="4"></div>
    <div class="grid-loader__col grid-loader__col--top" data-delay="5"></div>
    <div class="grid-loader__col grid-loader__col--top" data-delay="6"></div>
  </div>
  <!-- <div class="loader__gif"><img src="/affinity/sites/all/themes/custom/affinity/img/Preloader-Footprint_V2.gif" alt=""/></div> -->
</div>
