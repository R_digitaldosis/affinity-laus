<?php
global $base_path;
$theme = $base_path . 'sites/all/themes/custom/affinity';
$field = field_get_items('node', $node, 'field_cover_home');
$cover_node = node_load($field[0]['target_id']);
?>
<section class="grid grid-cover">
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-1" data-column="7" data-row="1"><div class="overpanel overpanel--white"></div></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-2" data-column="2" data-row="2"><div class="overpanel overpanel--white"></div></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile grey-3" data-column="6" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-mobile grey-4" data-column="1" data-row="5"></div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title"><?php echo render(field_view_field('node', $cover_node, 'field_prefix_cover')) ?></p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title">
        <h1 class="main-title"><?php $title_cover = field_get_items('node', $cover_node, 'field_title_cover'); echo $title_cover[0]['value'] ?></h1>
    </div>
    <?php $is_video_cover = field_get_items('node', $cover_node, 'field_has_video_cover');
    if( $is_video_cover[0]['value'] ){
      $video_ref = field_get_items('node', $cover_node, 'field_video_cover'); ?>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__video">
        <a href="<?php print $video_ref[0]['value']?>" title="" class="link-play-btn js-open-lightbox" data-lightbox="video">
            <div class="play-btn play-btn--roll-circle"><img src="<?php echo $theme; ?>/img/play.svg"></img></div>
            <p class="orange"><?php print t('Watch Video');?></p>
        </a>
    </div>
  <?php } ?>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__text-content text">
        <p>
          <?php echo render(field_view_field('node', $cover_node, 'field_text_cover')) ?>
        </p>
    </div>
    <?php
    $images_option = array("-a", "-b");
    for( $i=0;$i<5;$i++){
      $img_pos = rand(0, 1);
      $images[] = $images_option[$img_pos];
    }
    ?>
    <div class="grid__item grid-cover__item-cover__image grid-cover__item-cover__image--w2 grid-cover__item-cover__image--h2 grid__item--cube_1 is-desktop" data-column="6" data-row="1">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/home/home-img-01<?php echo $images[0];?>.jpg" alt="" />
          </picture>
        </div>
        <div class="overpanel"></div>
    </div>
    <div class="grid__item grid-cover__item-cover__image grid__item--cube_2 is-desktop" data-column="8" data-row="3">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-02@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/home/home-img-02.jpg, <?php echo $theme; ?>/img/home/home-img-02@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-02@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/home/home-img-02.webp, <?php echo $theme; ?>/img/home/home-img-02@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/home/home-img-02.jpg" alt="" />
          </picture>
        </div>
        <div class="overpanel"></div>
    </div>
    <div class="grid__item grid-cover__item-cover__image grid__item--cube_3 is-desktop" data-column="5" data-row="4">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>.jpg, <?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>.webp, <?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/home/home-img-03<?php echo $images[1];?>.jpg" alt="" />
          </picture>
        </div>
        <div class="overpanel"></div>
    </div>
    <div class="grid__item grid-cover__item-cover__image grid-cover__item-cover__image--w2 grid__item--cube_4 is-desktop" data-column="7" data-row="4">
        <div class="inner-image">
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-04@2x.jpg" type="image/jpeg" />
            <source srcset="<?php echo $theme; ?>/img/home/home-img-04.jpg, <?php echo $theme; ?>/img/home/home-img-04@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-04@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/home/home-img-04.webp, <?php echo $theme; ?>/img/home/home-img-04@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/home/home-img-04.jpg" alt="" />
          </picture>
        </div>
        <div class="overpanel"></div>
    </div>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to" data-scroll=".featured--about" data-column="6" data-row="5">
        <div class="orange btn-roll-bottom__label"><?php print t('View more');?></div>
    </div>
</section>
<?php
$field = field_get_items('node', $node, 'field_featured_pages');
$nodes_class = array('about', 'work-life', 'internships');
$features_img = array('06', '09', '11');
?>
<section class="featured-grid-cover">
  <div class="featured-grid grid">
    <?php for($i = 0 ; $i < 3 ; $i++){
      $node_id = $field[$i]['target_id'];
      $node_feature = node_load($node_id);
      $node_class = $nodes_class[$i];
      $data_number = $i+1 ;
      $img = $features_img[$i]; ?>
    <a href="<?php echo url('node/'.$node_id)?>" class="grid__item featured featured--<?php echo $node_class;?> js-exit-loader">
      <div class="featured__image">
        <div class="featured__zoom">  
          <picture class="featured__picture">
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>@2x.jpg" />
            <source srcset="<?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>.jpg, <?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>@2x.jpg 2x" type="image/jpeg" />
            <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>@2x.webp" type="image/webp" />
            <source
              srcset="<?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>.webp, <?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>@2x.webp 2x"
              type="image/webp"
            />
            <img src="<?php echo $theme; ?>/img/home/home-img-<?php echo $img;?>.jpg" alt="" />
          </picture>
        </div>
      </div>
      <div class="featured__content">
        <div class="featured__title title"><?php echo render($node_feature->title)?></div>
        <div class="featured__text text" data-number="<?php echo '0'.$data_number ?>">
          <p><?php echo render(field_view_field('node', $node_feature, 'field_featured_text_'.$i))?></p>
        </div>
      </div>
    </a>
  <?php } ?>
    <div class="grid__item grid__item--image image-1">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-05@2x.jpg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-05.jpg, <?php echo $theme; ?>/img/home/home-img-05@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-05@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-05.webp, <?php echo $theme; ?>/img/home/home-img-05@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-05.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid__item--image image-2">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>@2x.jpg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>.jpg, <?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>.webp, <?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-10<?php echo $images[3];?>.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid__item--image image-3">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-12@2x.jpg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-12.jpg, <?php echo $theme; ?>/img/home/home-img-12@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-12@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-12.webp, <?php echo $theme; ?>/img/home/home-img-12@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-12.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid__item--image image-4">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>@2x.jpg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>.jpg, <?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>.webp, <?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-13<?php echo $images[4];?>.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid__item--image image-5">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-07-a@2x.jpg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-07-a.jpg, <?php echo $theme; ?>/img/home/home-img-07-a@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-07-a@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-07-a.webp, <?php echo $theme; ?>/img/home/home-img-07-a@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-07-a.jpg" alt="" />
      </picture>
    </div>
    <div class="grid__item grid__item--image image-6">
      <picture class="featured__picture">
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>@2x.jpg" type="image/jpeg" />
        <source srcset="<?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>.jpg, <?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>@2x.jpg 2x" type="image/jpeg" />
        <source media="(min-width: 1440px)" srcset="<?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>@2x.webp" type="image/webp" />
        <source
          srcset="<?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>.webp, <?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>@2x.webp 2x"
          type="image/webp"
        />
        <img src="<?php echo $theme; ?>/img/home/home-img-08<?php echo $images[0];?>.jpg" alt="" />
      </picture>
    </div>

    <div class="grid__item grid__item--grey grey-1"></div>
    <div class="grid__item grid__item--grey grey-2"></div>
    <div class="grid__item grid__item--grey grey-3"></div>
    <div class="grid__item grid__item--grey grey-4"></div>
    <div class="grid__item grid__item--grey grey-5"></div>
    <div class="grid__item grid__item--grey grey-6"></div>
    <div class="grid__item grid__item--grey grey-7"></div>
    <div class="grid__item grid__item--grey grey-8"></div>
    <div class="grid__item grid__item--grey grey-9"></div>
    <div class="grid__item grid__item--grey grey-10"></div>
    <div class="grid__item grid__item--grey grey-11"></div>
    <div class="grid__item grid__item--grey grey-12 is-mobile"></div>
  </div>
</section>
 <section class="grid grid-find-your-job">
    <div class="grid__item grid__item--grey" data-column="1" data-row="1"></div>
    <div class="grid__item grid-find-your-job__item grid-find-your-job__item-title">
        <h2 class="title">
           <?php echo t('Find your next<br />challenge at Affinity!'); ?>
        </h2>
    </div>
    <a href="<?php echo url('/jobs'); ?>" class="grid__item grid-find-your-job__item grid-find-your-job__item-cta btn-roll-bottom js-exit-loader">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('See vacancies'); ?>
        </div>
    </a>
</section>
<?php
  drupal_add_js(array('theme' => $theme), 'setting');
  drupal_add_js($theme . '/js/animations.js', array(
    'scope' => 'footer',
    'weight' => 1
  ));
  drupal_add_js($theme . '/js/magic.js', array(
    'scope' => 'footer',
    'weight' => 2
  ));
 ?>
