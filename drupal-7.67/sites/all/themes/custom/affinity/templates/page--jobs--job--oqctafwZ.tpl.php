<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>

<div class="menu js-menu">
  <header class="header menu__header">
    <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

    <button class="header__burger hamburger hamburger--squeeze is-active js-close-menu" type="button">
      <span class="hamburger-box"> <span class="hamburger-inner"></span> </span>
    </button>
  </header>
  <nav class="menu__nav">
    <?php $mobile_menu = menu_navigation_links('menu-mobile-menu');
    print theme('links__system_menu_mobile_menu', array('links' => $mobile_menu) ); ?>
  </nav>
  <?php
    if (!isset($node)) {
      global $node;
    }
    $jobId = arg(2);

    $langList = language_list();
    global $language;
    global $base_url;
    $currentLang = $language->language;
  ?>
  <div class="menu__footer">
    <nav class="menu__languages">
      <?php foreach ($langList as $lang): ?>
      <?php

      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language == $currentLang): ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader is-active">
          <?php echo $lang->language; ?>
        </a>
      <?php else: ?>
        <a href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>" class="menu__language js-exit-loader">
          <?php echo $lang->language; ?>
        </a>
      <?php endif; ?>
     <?php endforeach; ?>
    </nav>
    <?php
	    $options = array('absolute' => TRUE);
	    $nid = 10; //cambiar por ID Legal Page
	    $url_legalPage = url('node/' . $nid, $options);
    ?>
    <div class="menu__copy">
      Affinity Petcare S.A.© Copyright <?php echo date('Y'); ?>. <br /><?php print t('All rights reserved.'); ?> <a href="<?php echo $url_legalPage;?>" class="js-exit-loader">Legal notice</a>
    </div>
  </div>
</div>
<div id="page">
<div class="header-wrapper js-header-wrapper">
<header class="header js-header" role="banner">

  <?php if ($logo): ?>
    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__link js-exit-loader"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo" /></a>
  <?php endif; ?>

  <nav class="header__externals">
    <?php
      $links = menu_navigation_links('menu-external');
      foreach ($links as $link): ?>
      <a href="<?php echo $link['href']; ?>" target="_blank" class="header__external">
        <?php echo $link['title']; ?>
      </a>
    <?php endforeach; ?>
  </nav>
  <!-- languages selector -->
  <div class="header__languages language-selector js-language-selector">
    <div class="language-selector__selected" data-selected="<?php echo $currentLang; ?>">
      <?php echo strtoupper($currentLang); ?>
    </div>
    <ul class="language-selector__languages">
      <?php foreach ($langList as $lang): ?>
      <?php
      if ($lang->prefix == '') {
        $prefix = '';
      } else {
        $prefix = $lang->prefix . '/';
      }
      if ($lang->language != $currentLang): ?>
        <li class="language-selector__language" data-language="<?php echo $lang->language; ?>">
          <a class="language-selector__link js-exit-loader" href="<?php echo url($base_url. '/'. $prefix . 'jobs/job/' . $jobId); ?>">
            <?php echo strtoupper($lang->language); ?>
          </a>
        </li>
      <?php endif; ?>
     <?php endforeach; ?>
    </ul>
  </div>

  <nav class="header__nav">
  <?php $main_menu = menu_navigation_links('main-menu');
  print theme('links__system_main_menu', array('links' => $main_menu) ); ?>
  </nav>
  <button class="header__burger hamburger hamburger--squeeze js-open-menu" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>

  <?php //print render($page['header']); ?>

</header>
</div>
<?php
  $job = d26jobs_get_requisition($jobId);
?>
<section class="grid grid-cover grid-cover--offer">
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-1" data-column="7" data-row="1"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-2" data-column="2" data-row="2"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-3" data-column="6" data-row="4"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey grey-4" data-column="1" data-row="5"></div>
    <div class="grid__item grid__item--grey grid-cover__item-cover grid-cover__item-cover--grey is-desktop grey-5" data-column="8" data-row="3"></div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__line" data-column="2" data-row="4">
        <div class="decoration-line"></div>
    </div>
    <div class="grid__item grid-cover__item-cover grid-cover__item-cover__text grid-cover__item-cover__pretitle">
        <p class="pre-main-title">
          <?php echo t('JOIN US!'); ?>
        </p>
    </div>
    <div class="grid__item grid-cover__item-cover__text grid-cover__item-cover__title">
        <h1 class="main-title">
          Employer Brand Manager
        </h1>
        <h2 class="main-subtitle">
          Spain (L’Hospitalet de Llobregat)
        </h2>
    </div>
    <a href="#" class="grid__item cta cta--reversed js-scroll-to is-mobile" data-scroll=".offer__footer">
      <?php echo t('Apply'); ?>
    </a>
    <div class="grid__item grid-cover__item-cover__viewmore btn-roll-bottom is-desktop js-scroll-to complete-show" data-scroll=".offer__footer" data-column="7" data-row="3">
        <div class="orange btn-roll-bottom__label">
          <?php echo t('Apply'); ?>
        </div>
    </div>
</section>
<section class="offer">
  <div class="offer__content">
    <p><strong>What we offer</strong></p><p>We are looking for a creative and digital marketing professional to be the leader of Affinity Petcare employer brand across Spain, Italy and France.</p><p>You will be the digital expert and champion of sharing Affinity Petcare company purpose, initiatives, employee stories and EVP (Employee Value Proposition) to build brand awareness both externally and internally.</p><p>You will be the owner of the company-wide internal communication, gathering most relevant content to boost employee sense of belonging and unite all sites together.</p><p>To be the best in your role, you will build strong partnerships with internal stakeholders across our Business Units and Global Units.</p><p>&nbsp;</p><p><strong>Affinity Benefits</strong></p><p>Little things make a big difference. That’s why we want you to have the best of our care so you can leave your footprint every day. In Affinity you’ll have plenty of benefits:</p><ul><li>Healthy breakfasts</li><li>Meal tickets</li><li>Fruit all day</li><li>Gym</li><li>Flexible compensation</li><li>Pets welcome</li><li>Fun &amp; celebrations</li><li>Pension plan</li></ul><p>&nbsp;&nbsp;</p><p><strong>What you will do</strong></p><p><span style="text-decoration: underline;">Define and follow up the Employer Branding strategy (Talent Brand)</span></p><ul><li>Collaborate with creative agency to design and roll out talent attraction campaigns both for digital and recruitment fairs.</li><li>Interact and work with creative agencies for content creation and ensure delivery of monthly Content Plan.</li><li>Manage and follow up paid marketing campaigns on social media.</li><li>Keep information and content up to date on social media (Instagram, Facebook and LinkedIn, including LinkedIn Life pages) and careers website.</li><li>Make sure the candidate funnel is in place and that we deliver the right messages providing a good hiring candidate experience.</li><li>Analyse data and reports to extract actionable insights to improve brand strategy.</li><li>Be a proud #WeAreAffinity ambassador.</li></ul><span style="text-decoration: underline;">Lead Life at Affinity initiatives to engage employees</span><br><ul><li>Take care of internal communications to employees of Life at Affinity initiatives.</li><li>Source and draft content for company-wide internal communication (Affinity News).</li><li>Gather content from employees to include in the monthly Content Plan.</li></ul>&nbsp;<br><p><strong>What we are looking for…</strong></p><ul><li>College degree.</li><li>3-5 years’ experience in marketing.</li><li>Previous experience in digital, content marketing, social media and branding.</li><li>Strong organizational and project management skills.</li><li>Ability to work independently, exercise good judgment, adapt quickly and prioritize.</li><li>Inspire others – ability to engage broad range of partners through good interpersonal skills, empathy and ability to deliver.</li><li>Excellent communication and writing skills.</li><li>High level of English and Spanish, other languages such as French or Italian are a plus.</li><li>Bonus points if you have basic graphic design knowledge.</li></ul>
  </div>
  <div class="offer__footer">
    <h2 class="offer__title">
      <?php echo t('Submit your application,');?>
    </h2>
    <div class="offer__text">
      <p>
        <?php echo t("we are ready to meet you!"); ?>
      </p>
    </div>
    <a href="#" class="offer__apply">
      <?php echo t('Apply'); ?>
    </a>
  </div>
</section>

<?php print render($page['footer']); ?>



<?php print render($page['bottom']);?>
