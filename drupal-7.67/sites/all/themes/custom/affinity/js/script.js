'use strict';
(function($, Drupal, window, document) {

  var breakpoint = window.getCurrentBreakpoint();
  var header = 0;
  var sliders = {};
  var lastScrollTop = 0;
  var video_ref;
  var filters = {
    location: [],
    category: []
  };

  var scrollbar;
  var offset = 0;
  var scenes = [];
  var fixedElements = [
    '.header-wrapper.is-fixed',
  ];
  var lastScrollTop = 0;
  var headerScroll = function(st) {
    if (st > lastScrollTop && st >= header) {
      $('.js-header').addClass('is-up');
      setTimeout(function() {
        $('.js-header, .js-header-wrapper').addClass('is-fixed');
      }, 200);
    } else if (st === 0) {
      $('.js-header, .js-header-wrapper').removeClass('is-up is-fixed');
      setTimeout(function() {
        $('.js-header').removeClass('is-fixed');
      }, 200);
    } else {
      $('.js-header').removeClass('is-up');
    }
    lastScrollTop = st;
  }
  var positionFix = function() {
    fixedElements.forEach(function(selector) {
      var $element = $(selector);
      if ($element.css('position') === 'fixed') {
        $element.css('transform', 'translate3d(0, ' + offset + 'px' + ',0)');
      } else {
        $element.css('transform', 'translate3d(0,0,0)');
      }
    })
  };
  var enableScroll = function() {
    window.Scrollbar.use(window.OverscrollPlugin);
    scrollbar = window.Scrollbar.init(document.querySelector('#page'), {
      continuousScrolling: false,
      plugins: {
        overscroll : {
          effect: 'bounce',
          damping: 0.1,
          maxOverscroll: 220
        }
      }
    });

    scrollbar.addListener(function(params) {

      offset = params.offset.y;
      window.controller.scenes.forEach(function(scene) {
        scene.refresh();
      })
      positionFix(offset);
      if (!window.mediaQuery('desktop')) {
        headerScroll(offset);
      }
      // window.scrollHandler(offset);
    });
  };
  // init controller
  var Controller = function() {
    this.scenes = [];
    this.scrollController = new ScrollMagic.Controller({
      refreshInterval: 0
    });
    this.addScene = function(scene) {
      if (typeof scene === 'object') {
        this.scenes = this.scenes.concat(scene);
      } else {
        this.scenes.push(scene);
      }
      this.scrollController.addScene(scene);
    }
  }
  window.controller = new Controller();

  var applyFilters = function() {
    $('.js-opening').addClass('is-hidden');
    $('.js-no-results').removeClass('is-visible');

    filters.location.forEach(function(location) {
      if (filters.category.length) {
        filters.category.forEach(function(category) {
          $('.js-opening[data-location="' + location + '"][data-category="' + category + '"]').removeClass('is-hidden');
        });
      } else {
        $('.js-opening[data-location="' + location + '"]').removeClass('is-hidden');
      }
    });

    filters.category.forEach(function(category) {
      if (filters.location.length) {
        filters.location.forEach(function(location) {
          $('.js-opening[data-location="' + location + '"][data-category="' + category + '"]').removeClass('is-hidden');
        });
      } else {
        $('.js-opening[data-category="' + category + '"]').removeClass('is-hidden');
      }
    });

    if (!filters.location.length && !filters.category.length) {
      $('.js-opening').removeClass('is-hidden');
    }
    if (!$('.js-opening:visible').length) {
      $('.js-no-results').addClass('is-visible');
    }
  };

  // TODO: trigger on content load
  var showLoader = function() {
    //$('.loader__gif').show();
    setTimeout(function() {
      $('.js-loader').addClass('loading');
    }, 500);
    /*setTimeout(function() {
      $('.loader__gif').hide();
    }, 800);*/
    setTimeout(function() {
      $('.js-loader')
        .removeClass('loading')
        .addClass('loaded loading--exit');
    }, 1500);
  };
  var exitLoader = function() {
    $('.js-loader').removeClass('loaded loading--exit');
    /*setTimeout(function() {
      $('.loader__gif').show();
    }, 500);*/
  };

  var disableArrows = function(slider, arrow) {
    var $prev = $(arrow + '-prev');
    var $next = $(arrow + '-next');
    $prev.removeClass('is-disabled');
    $next.removeClass('is-disabled');
    if (slider.isBeginning) {
      $prev.addClass('is-disabled');
    }
    if (slider.isEnd) {
      $next.addClass('is-disabled');
    }
  };

  var historySwiper = function() {
    if ($('.js-history-slider').length) {
      if (sliders.history) {
        sliders.history.destroy(true, true);
      }
      sliders.history = new window.Swiper('.js-history-slider', {
        slidesPerView: 'auto',
      });
      sliders.history.on('slideChangeTransitionEnd', function() {
        var direction = this.previousIndex < this.realIndex ? 'adelante' : 'atras';
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_carrusel:clic',
          'data_1': 'our history',
          'data_2': direction
        });
      });
      disableArrows(sliders.history, '.js-history-slider');
    }
  };
  var testimonialSwiper = function() {
    if ($('.js-testimonials-slider').length) {
      if (sliders.testimonial) {
        sliders.testimonial.destroy(true, true);
      }
      sliders.testimonial = new window.Swiper('.js-testimonials-slider', {
        slidesPerView: 'auto',
      });
      sliders.testimonial.on('slideChangeTransitionEnd', function() {
        var direction = this.previousIndex < this.realIndex ? 'adelante' : 'atras';
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_carrusel:clic',
          'data_1': 'testimonials',
          'data_2': direction
        });
      });
      disableArrows(sliders.testimonial, '.js-testimonials-slider');

    }
  };

  var hiringSwiper = function() {
    if ($('.js-hiring-slider').length) {
      if (sliders.hiring) {
        sliders.hiring.destroy(true, true);
      }
      sliders.hiring = new window.Swiper('.js-hiring-slider', {
        slidesPerView: 'auto',
      });
      sliders.hiring.on('slideChangeTransitionEnd', function() {
        var direction = this.previousIndex < this.realIndex ? 'adelante' : 'atras';
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_carrusel:clic',
          'data_1': 'hiring steps',
          'data_2': direction
        });
      });
      disableArrows(sliders.hiring, '.js-hiring-slider');
    }
  };

  var resize = function() {
    if (breakpoint !== window.getCurrentBreakpoint()) {
      historySwiper();
    }
  };

  var initAnalytics = function() {
    window.dataLayer = window.dataLayer || [];

    var urlEs = $('.language-selector__language[data-language="es"] a').attr('href') || window.location.href;
    var lang = $('html').attr('lang');
    window.dataLayer.push({
    'url_es': urlEs,
    'idioma': lang
    });
  };

  var detectScroll = function() {
    var st = $(this).scrollTop();

    if (st > lastScrollTop && st >= header) {
      $('.js-header').addClass('is-up');
      setTimeout(function() {
        $('.js-header').addClass('is-fixed');
      }, 200);
    } else if (st <= 2) {
      $('.js-header').removeClass('is-up is-fixed');
      setTimeout(function() {
        $('.js-header').removeClass('is-fixed');
      }, 200);
    } else {
      $('.js-header').removeClass('is-up');
    }
    lastScrollTop = st;
  };
  var showCookies = function() {
    if (document.cookie.indexOf('accept=true') === -1) {
      $('.js-cookies').addClass('is-visible');
    }
  };

  var scrollTo = function(scroll) {
    scrollbar.scrollTo(0, scroll + offset, 500);
    // $('html,body').stop().animate(
    //   {
    //     scrollTop: offset,
    //   },
    //   500
    // );
  }

  $(document).ready(function() {
    window.onscroll = detectScroll;
    header = $('.js-header').height();
    historySwiper();
    testimonialSwiper();
    hiringSwiper();
    showLoader();
    initAnalytics();
    showCookies();
    enableScroll();
  });

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.affinity_openMenu = {
    attach: function(context, settings) {
      // Open menu
      $('.js-open-menu', context).click(function(e) {
        e.preventDefault();
        $(this).addClass('is-active');
        $('.js-close-menu').addClass('is-active');
        $('.js-menu').addClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_closeMenu = {
    attach: function(context, settings) {
      // Close menu
      $('.js-close-menu', context).click(function(e) {
        e.preventDefault();
        $(this).removeClass('is-active');
        $('.js-open-menu').removeClass('is-active');
        $('.js-menu').removeClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_languageSelector = {
    attach: function(context, settings) {
      // Language selector
      $('.js-language-selector', context).click(function(e) {
        // e.preventDefault();
        $(this).toggleClass('is-selected');
      });
    }
  };
  Drupal.behaviors.affinity_resize = {
    attach: function(context, settings) {
      // Window resize
      $(window, context).resize(function() {
        header = $('.js-header').height();
        resize();
        breakpoint = window.getCurrentBreakpoint();
      });
    }
  };
  Drupal.behaviors.affinity_historyPrev = {
    attach: function(context, settings) {
      // History slider prev
      $('.js-history-slider-prev', context).click(function() {
        if (sliders.history) {
          sliders.history.slidePrev();
          disableArrows(sliders.history, '.js-history-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_historyNext = {
    attach: function(context, settings) {
      // History slider next
      $('.js-history-slider-next', context).click(function() {
        if (sliders.history) {
          sliders.history.slideNext();
          disableArrows(sliders.history, '.js-history-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_testimonialsPrev = {
    attach: function(context, settings) {
      // Testimonials slider prev
      $('.js-testimonials-slider-prev', context).click(function() {
        if (sliders.testimonial) {
          sliders.testimonial.slidePrev();
          disableArrows(sliders.testimonial, '.js-testimonials-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_testimonialsNext = {
    attach: function(context, settings) {
      // Testimonials slider next
      $('.js-testimonials-slider-next', context).click(function() {
        if (sliders.testimonial) {
          sliders.testimonial.slideNext();
          disableArrows(sliders.testimonial, '.js-testimonials-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_hiringPrev = {
    attach: function(context, settings) {
      // Hiring slider prev
      $('.js-hiring-slider-prev', context).click(function() {
        if (sliders.hiring) {
          sliders.hiring.slidePrev();
          disableArrows(sliders.hiring, '.js-hiring-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_hiringNext = {
    attach: function(context, settings) {
      // Hiring slider next
      $('.js-hiring-slider-next', context).click(function() {
        if (sliders.hiring) {
          sliders.hiring.slideNext();
          disableArrows(sliders.hiring, '.js-hiring-slider');
        }
      });
    }
  };
  Drupal.behaviors.affinity_casesToggleText = {
    attach: function(context, settings) {
      //internships-cases show text
      $('.grid-opportunities-cases__arrow-down').click(function() {
        $(this)
          .closest('.grid-opportunities-cases')
          .toggleClass('grid-opportunities-cases--extended');
      });
    }
  };
  Drupal.behaviors.affinity_openLightbox = {
    attach: function(context, settings) {
      // Open lightbox
      $('.js-open-lightbox', context).click(function(e) {
        e.preventDefault();
        video_ref = $(this).attr('href');
        video_ref = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' + video_ref + '?version=3&rel=0&controls=0&disablekb=1&fs=0&autoplay=1&showinfo=0" frameborder="0" allowfullscreen></iframe>';
        $('.js-lightbox[data-lightbox="' + $(this).data('lightbox') + '"]').find('.lightbox__body').html(video_ref);
        $('.js-lightbox[data-lightbox="' + $(this).data('lightbox') + '"]').addClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_closeLightbox = {
    attach: function(context, settings) {
      // Close lightbox
      $('.js-close-lightbox', context).click(function() {
        $('.js-lightbox').removeClass('is-visible');
        $(".js-lightbox[data-lightbox='video']").find('.lightbox__body').html('');
      });
    }
  };
  Drupal.behaviors.affinity_officesHover = {
    attach: function(context, settings) {
      // Offices hover
      $('.js-location-hover', context).mouseenter(function() {
        $('.js-location-hover').addClass('is-gray');
        $('.js-location-hover[data-location="' + $(this).data('location') + '"]').removeClass('is-gray');
      });
      $('.js-location-hover', context).mouseleave(function() {
        $('.js-location-hover').removeClass('is-gray');
      });
    }
  };
  Drupal.behaviors.affinity_scrollTo = {
    attach: function(context, settings) {
      // Scroll to
      $('.js-scroll-to', context).click(function(e) {
        var selector = $(this).data('scroll');
        scrollTo($(selector).offset().top);
        e.preventDefault();
      });
    }
  };
  Drupal.behaviors.affinity_exitLoader = {
    attach: function(context, settings) {
      // Loader exit
      $('.js-exit-loader', context).click(function(e) {
        var $link = $(this).prop('tagName') === 'A' ? $(this) : $(this).find('a').eq(0) ;
        e.preventDefault();
        exitLoader();
        setTimeout(function() {
          window.location.href = $link.attr('href');
        }, 1500);
      });
    }
  };
  Drupal.behaviors.affinity_filterChange = {
    attach: function(context, settings) {
      // Career filters
      $('.js-filter', context).change(function() {
        var filter = Object.keys(this.dataset)[0];
        var value = this.dataset[filter];

        if ($(this).prop('checked')) {
          if ($.inArray(value, filters[filter]) === -1) {
            filters[filter].push(value);
          }
        } else {
          var index = filters[filter].indexOf(value);
          filters[filter].splice(index, 1);
        }

        if (breakpoint === 'desktop') {
          applyFilters();
        }
      });

      // Mobile career filters
      $('.js-apply-filters').click(function(e) {
        e.preventDefault();
        applyFilters();
      });
      $('.js-open-filters').click(function(e) {
        $('.js-filters-popup').addClass('is-visible');
      });
      $('.js-close-filters').click(function(e) {
        $('.js-filters-popup').removeClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_filterApply = {
    attach: function(context, settings) {
      // Mobile career filters
      $('.js-apply-filters', context).click(function(e) {
        e.preventDefault();
        applyFilters();
      });
    }
  };
  Drupal.behaviors.affinity_filterOpen = {
    attach: function(context, settings) {
      $('.js-open-filters', context).click(function(e) {
        $('.js-filters-popup').addClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_filterClose = {
    attach: function(context, settings) {
      $('.js-close-filters', context).click(function(e) {
        $('.js-filters-popup').removeClass('is-visible');
      });
    }
  };
  Drupal.behaviors.affinity_analytics = {
    attach: function(context, settings) {

      // Main menu
      $('.header__nav-link a', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_menu_principal:clic',
          'data_1': $(this).text()
        });
      });

      // Mobile menu
      $('.menu__link a', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_menu_principal:clic',
          'data_1': $(this).text()
        });
      });
      // Mobile menu icon
      $('.js-open-menu', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_menu_principal:clic',
          'data_1': 'menu responsive'
        });
      });

      // Slider testimonials next
      $('.testimonials__arrow--next', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'testimonials',
        //   'data_2': 'adelante'
        // });
      });

      // Slider testimonials prev
      $('.js-testimonials-slider-prev', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'testimonials',
        //   'data_2': 'atras'
        // });
      });

      // Slider hiring next
      $('.grid-hiring__arrow--next', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'hiring steps',
        //   'data_2': 'adelante'
        // });
      });

      // Slider hiring prev
      $('.grid-hiring__arrow--prev', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'hiring steps',
        //   'data_2': 'atras'
        // });
      });

      // Slider history next
      $('.our-history__arrow--next', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'our history',
        //   'data_2': 'adelante'
        // });
      });

      // Slider history prev
      $('.js-history-slider-prev', context).click(function() {
        // window.dataLayer.push({
        //   'event': 'adv_event',
        //   'event_custom': 'waf_carrusel:clic',
        //   'data_1': 'our history',
        //   'data_2': 'atras'
        // });
      });

      // CTA apply
      $('.offer__apply', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_aplicar_ofertas:clic',
          'data_1': $('.main-title').text().replace(/\s+/,'').replace(/\s+$/g, '')
        });
      });

      // CTA find job
      $('.grid-find-your-job .btn-roll-bottom', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_oportunidades:clic'
        })
      });

      // Social Link
      $('.grid-footer__social a', context).click(function() {
        var $social = $(this).attr("title") === 'linkedin' ? 'linkedin' : 'instagram';
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_redes_sociales:clic',
          'data_1': $social
        })
      });

      //Selección de idiomas
      $('.menu__languages a, .language-selector__language a', context).click(function() {
        var $lang = $(this).text();
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_seleccion_idioma:clic',
          'data_1': $lang
        })
      });

      //Selección de idiomas
      $('.grid-cover__item-cover__viewmore', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_ver_mas:clic'
        })
      });

      //Jobs
      $('.js-opening', context).click(function() {
        window.dataLayer.push({
          'event': 'adv_event',
          'event_custom': 'waf_ver_ofertas:clic',
          'data_1': $(this).find('.opening__title').text().trim()
        })
      });
    }
  };
  Drupal.behaviors.affinity_filterApply = {
    attach: function(context, settings) {
      // Accept cookies
      $('.js-accept-cookies', context).click(function() {
        document.cookie = 'accept=true';
        $('.js-cookies').removeClass('is-visible');
      });
    }
  };
})(jQuery, Drupal, this, this.document);
