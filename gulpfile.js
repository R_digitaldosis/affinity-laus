'use strict';

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  rename = require('gulp-rename'),
  plumber = require('gulp-plumber'),
  cssnano = require('gulp-cssnano'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer');

var config = require('./config.json'),
  destination = 'dist';

// general vendor css
gulp.task('css-vendors', function() {
  return gulp
    .src([
      'node_modules/swiper/dist/css/swiper.min.css',
      // 'node_modules/pace-progress/themes/blue/pace-theme-center-simple.css'
    ])
    .pipe(browserSync.stream())
    .pipe(concat('vendors.min.css'))
    .pipe(gulp.dest(destination + '/css'));
});

// vendors
gulp.task('vendors', function() {
  return gulp
    .src([
      'node_modules/jquery/dist/jquery.min.js',
      'node_modules/swiper/dist/js/swiper.min.js',
      'node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
      'node_modules/gsap/src/minified/TweenMax.min.js',
      'node_modules/gsap/src/minified/TimelineMax.min.js',
      'libs/CustomEase.js',
      'node_modules/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js',
      'node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addindicators.min.js',
      'node_modules/smooth-scrollbar/dist/smooth-scrollbar.js',
      'node_modules/smooth-scrollbar/dist/plugins/overscroll.js'
    ])
    .pipe(plumber())
    .pipe(concat('vendors.min.js'))
    .pipe(gulp.dest(destination + '/js'));
});

// browserSync setup
gulp.task('browser-sync', function() {
  return browserSync.init({
    server: {
      baseDir: './',
    },
    open: true,
  });
});

//Compile scss files
gulp.task('sass', function() {
  var processors = [
    autoprefixer({
      browsers: ['last 2 versions', 'ie >= 10'],
      cascade: false,
      grid: true,
    }),
  ];
  gulp
    .src('scss/**/*.scss')
    .pipe(
      sass({
        outputStyle: 'compressed',
      }).on('error', sass.logError)
    )
    .pipe(
      rename({
        suffix: '.min',
      })
    )
    .pipe(
      cssnano({
        zindex: false,
        reduceTransforms: false,
      })
    )
    .pipe(postcss(processors))
    .pipe(browserSync.stream())
    .pipe(gulp.dest(destination + '/css'));
});

// watch files for live reload
gulp.task('watch', function() {
  gulp.watch('scss/**/*.scss', ['sass']);

  gulp
    .watch([
      destination + '/**/*.js',
      destination + '/**/*.html',
      destination + '/**/*.php',
      destination + '/**/*.twig',
    ])
    .on('change', browserSync.reload);
});

// ASCII art easter egg
/* eslint-disable */
var rocks = function() {
  var guitar =
    '             _                        _         _ \r\n  __ _ _   _| |_ __    _ __ ___   ___| | _____ | |\r\n / _` | | | | | \'_ \\  | \'__/ _ \\ / __| |/ / __|| |\r\n| (_| | |_| | | |_) | | | | (_) | (__|   <\\__ \\|_|\r\n \\__, |\\__,_|_| .__/  |_|  \\___/ \\___|_|\\_\\___/|_|\r\n |___/        |_|                                                                   \r\n                                     \r\n       ._-_.    ________________________\r\n      +|\\G/|+  | _______________________|\r\n      +|\\./|+  || O  o o o  =|=  |   o ||\r\n      +|\\./|+  || O  o o o   |  =|=  o ||\r\n       `|H|\'   ||______________________||\r\n        |a|    |________________________| \r\n        |H|    ||MM88MM<<<?<<<XHHHHMMMM||  \r\n        |a|    ||M88MM<<<?<<<XHHHMMMMMM||  \r\n        |H|    ||88MM<<<?<<<XHHHMMMMMMM||  \r\n        |a|    ||8MM<<<?<<<XHHHHMMMMMMM||  \r\n        |H|    ||MM<<<?<<<XHHHHMMMMMMMM||  \r\n        |H|    ||M<<<?<<<XHHHHMMMMMMMMM||  \r\n  _-_   |H|   _-_<<<?<<<XHHHHMMMMMMMMMM||  \r\n /   \\  |H|  /   \\<?<<<XHHHHMMMMMMMMMMM||  \r\n |    \\_|a|_/    |?<<<XHHHHMMMMMMMMMMMM||  \r\n \\      |H|      /<<<XHHHHMMMMMMMMMMMMR||  \r\n  \\     |H|     /<<<XHHHHMMMMMMMMMMMRMM||  \r\n   |    \'"\'    |<<<XHHHHMMMMMMMMMMMRMM8||  \r\n  /     ===     \\<XHHHHMMMMMMMMMMMRMM8R||  \r\n /      ===   !  \\HHHHMMMMMMMMMMMRMM8RM||  \r\n|             | o |HMMMMMMMMMMMMMM988MM||  \r\n|      +---+ /  o |MMMMMMMMMMMMMM988MM<||  \r\n\\       ___ /  o  /M/MMMMMRMMMRMM88MM<<||  \r\n \\     |HHH|    l/MMMMMMMRMMMRMM88MM<<<||  \r\n  `-_   \\_/   _-MMRMMMMMRMMMRMM88MM<<<?||  \r\n     """"""""\' ~~~V~~""~~~~~~~~~~~~~~V~~~  \r\n';
  console.log(guitar);
};
/* eslint-enable */

// tasks
gulp.task('build', ['vendors', 'css-vendors', 'sass']);
gulp.task('rocks', ['build', 'browser-sync', 'watch'], rocks);
